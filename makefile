kgccargs = -Wall -Wsuggest-attribute=pure -Wsuggest-attribute=const -m32 -mno-sse -Og -ggdb -ffreestanding -fno-asynchronous-unwind-tables -fno-pic -Isrc/shared/include -Isrc/lib
ugccargs = ${kgccargs} -Isrc/user/include
ugppargs = ${ugccargs} -fno-rtti -fno-exceptions -Isrc/user/include/cxx
nasmargs = -f elf32
partlink = -r -m elf_i386
clink = -T src/user/runtimes/c/elf.ld
cxxlink = ${clink} src/user/runtimes/cxx/extra.ld
asmlink = -T src/user/runtimes/asm/elf.ld

out/disk.vdi: out/disk.img
	rm out/disk.vdi || true
	VBoxManage convertfromraw out/disk.img out/disk.vdi --uuid a61929ed-3bf2-45ff-b98a-44f87c616dba

out/disk.img: out/kernel.bin out/boot.bin out/fs
	mkdir -p obj
	mkfs.fat -C -f 1 -F 16 -n "PORTLAND OS" -R 129 -s 1 -S 512 obj/shadow.img 8192
	echo -n -e '\x01\x00\x00\x00uid \x00\x00\x00\x00' > out/fs/_fstab
	dd if=obj/shadow.img of=out/fs/_fstab bs=1 seek=4 skip=39 count=4 conv=notrunc
	echo -n -e '\xeb\x3c' > obj/jmp.bin
	dd if=obj/jmp.bin of=obj/shadow.img bs=1 conv=notrunc
	dd if=out/boot.bin of=obj/shadow.img bs=1 seek=62 conv=notrunc
	dd if=out/kernel.bin of=obj/shadow.img seek=1 conv=notrunc
	mv obj/shadow.img out/disk.img
	mcopy -i out/disk.img -s out/fs/* ::/

clean:
	rm -r obj out || true

gdb: out/disk.img
	gdb -x qemu.gdb

ddd: out/disk.img
	ddd --debugger 'gdb -x qemu.gdb'

out/fs/bin/%: obj/%.elf
	mkdir -p $(shell dirname $@)
	objcopy -S $< $@

#out/fs/man/%.man: src/man/%.pre
#	mkdir -p $(shell dirname $@)
#	python3 tools/man-gen.py $< $@

out/fs: out/fs/bin/init     out/fs/bin/highway out/fs/bin/meminfo \
        out/fs/bin/terminal out/fs/bin/hello   out/fs/bin/mkpopup \
        out/fs/bin/dirlist  out/fs/bin/time \
        out/fs/bin/filetest out/fs/bin/settings
	touch out/fs
	cp -r fs-skel/* out/fs/

obj/kernel/%.ko: src/kernel/%.c
	mkdir -p $(shell dirname $@)
	gcc ${kgccargs} -c $< -o $@

obj/kernel/%.kao: src/kernel/%.asm
	mkdir -p $(shell dirname $@)
	nasm ${nasmargs} $< -o $@

out/kernel.bin: obj/kernel/drive.ko  obj/kernel/fat.ko      obj/kernel/ide.ko  \
                obj/kernel/idt.ko    obj/kernel/log.ko      obj/kernel/main.ko \
                obj/kernel/panic.ko  obj/kernel/pci.ko      obj/kernel/elf.ko  \
                obj/kernel/serial.ko obj/kernel/task.ko     obj/kernel/util.ko \
                obj/kernel/window.ko obj/kernel/isrs.kao    obj/kernel/kbd.ko  \
                obj/kernel/pmap.ko   obj/kernel/paging.ko   obj/kernel/dump.ko \
                obj/kernel/cmos.ko   obj/kernel/settings.ko obj/kernel/shutdown.ko
	mkdir -p out
	ld -T src/kernel/elf-link.ld $^ -o obj/kernel.elf
	objcopy -O binary obj/kernel.elf out/kernel.bin

out/boot.bin: src/boot.asm
	mkdir -p out
	nasm src/boot.asm -o out/boot.bin

obj/%.o: src/user/%.c
	mkdir -p $(shell dirname $@)
	gcc ${ugccargs} -c $< -o $@

obj/%.ao: src/user/%.asm
	mkdir -p $(shell dirname $@)
	nasm ${nasmargs} $< -o $@

obj/%.po: src/user/%.cpp
	mkdir -p $(shell dirname $@)
	g++ ${ugppargs} -c $< -o $@

obj/c.rto: obj/runtimes/c/pcrt.ao
	ld ${partlink} $^ -o $@

obj/cxx.rto: obj/c.rto \
             obj/runtimes/cxx/extra.ao
	ld ${partlink} $^ -o $@

obj/knob.so: obj/knob/file.o  obj/knob/format.o obj/knob/rand.o  \
             obj/knob/heap.o  obj/knob/ipc.o    obj/knob/task.o  \
             obj/knob/block.o obj/knob/key.o    obj/knob/panic.o \
             obj/knob/time.o
	ld ${partlink} $^ -o $@

obj/libterm.so: obj/libterm/terminal.o obj/libterm/termtask.o obj/libterm/readline.o
	ld ${partlink} $^ -o $@

obj/libfont.so: obj/libfont/bdf.o obj/libfont/pbf.o obj/libfont/fonts.o \
                obj/libfont/filist.o
	ld ${partlink} $^ -o $@

obj/raleigh.so: obj/raleigh/runtime.po   obj/raleigh/window.po           \
                obj/raleigh/widget.po    obj/raleigh/util.po             \
                obj/raleigh/w/padding.po obj/raleigh/w/button.po         \
                obj/raleigh/w/vbox.po    obj/raleigh/w/entry.po          \
                obj/raleigh/w/label.po   obj/raleigh/w/colorpicker.po    \
                obj/raleigh/w/hbox.po    obj/raleigh/w/multicontainer.po \
                obj/raleigh/d/dialog.po  obj/raleigh/d/saving_window.po  \
                obj/raleigh/s/text_flower.po
	ld ${partlink} $^ -o $@

obj/init.elf: obj/init/init.o obj/knob.so obj/c.rto
	ld ${clink} $^ -o $@

obj/highway.elf: obj/highway/main.o obj/highway/cmds.o obj/highway/line.o \
                 obj/highway/vars.o obj/knob.so        obj/libterm.so     \
                 obj/c.rto
	ld ${clink} $^ -o $@

obj/meminfo.elf: obj/meminfo/meminfo.po obj/raleigh.so obj/libfont.so \
                 obj/knob.so            obj/cxx.rto
	ld ${cxxlink} $^ -o $@

obj/terminal.elf: obj/terminal/main.o obj/libfont.so obj/knob.so \
                  obj/c.rto
	ld ${clink} $^ -o $@

obj/hello.elf: obj/hello/hello.ao
	ld ${asmlink} $^ -o $@

obj/mkpopup.elf: obj/mkpopup/main.po obj/raleigh.so obj/libfont.so \
                 obj/knob.so         obj/cxx.rto
	ld ${cxxlink} $^ -o $@

obj/dirlist.elf: obj/dirlist/main.o obj/libterm.so obj/knob.so \
                 obj/c.rto
	ld ${clink} $^ -o $@

obj/time.elf: obj/time/time.o obj/libterm.so obj/knob.so \
              obj/c.rto
	ld ${clink} $^ -o $@

obj/filetest.elf: obj/filetest/filetest.o obj/libterm.so obj/knob.so \
                  obj/c.rto
	ld ${clink} $^ -o $@

obj/settings.elf: obj/settings/main.po        obj/settings/model.po \
                  obj/settings/string_kind.po obj/settings/color_kind.po \
                  obj/raleigh.so obj/libfont.so \
                  obj/knob.so    obj/cxx.rto
	ld ${cxxlink} $^ -o $@