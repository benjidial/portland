#ifndef SHARED_WINACT_H
#define SHARED_WINACT_H

#include <keypack.h>

struct mouse_packet {
  uint16_t y;
  uint16_t x;
  enum mouse_button {LEFT, RIGHT, MIDDLE} which;
} __attribute__ ((__packed__));

struct window_action {
  enum {
    NOT_READY,
    KEY_DOWN,
    KEY_UP,
    FOCUS_ENTER,
    FOCUS_LEAVE,
    MOUSE_DOWN,
    MOUSE_UP,
    MOUSE_MOVE
  } action_type;
  union {
    struct key_packet as_key;
    struct mouse_packet as_mouse;
    struct {
      uint32_t y;
      uint32_t x;
    } moved_to;
  };
} __attribute__ ((__packed__));

#endif