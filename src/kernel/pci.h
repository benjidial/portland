#ifndef PCI_H
#define PCI_H

#include <stdint.h>

enum {
  PCI_MASS_STORAGE = 0x01
};

enum {
  PCI_IDE = 0x01
};

struct pci_device {
  uint16_t number;

  uint16_t id_vendor;
  uint16_t id_device;

  uint8_t class;
  uint8_t subclass;
  uint8_t iface;
};

extern uint16_t n_pci_devices;
struct pci_device *nth_pci_device(uint16_t n) __attribute__ ((pure));

struct pci_device *find_pci_device_from_class_and_subclass(uint8_t class, uint8_t subclass, uint16_t start, uint16_t *index);

void pci_init();

#endif