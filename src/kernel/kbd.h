#ifndef KBD_H
#define KBD_H

#include <stdbool.h>
#include <stdint.h>

#include <keypack.h>

void init_kbd();

enum kbd_isr_result {
  NORMAL,
  DUMP,
  SHIFT_DUMP
} on_kbd_isr();

extern enum key_modifiers_t keymods;

#endif
