#ifndef UTIL_H
#define UTIL_H

#include <stdint.h>
#include "drive.h"

static inline void outb(uint16_t port, uint8_t value) {
  asm (
    "outb %0, %1"
  : : "a"(value), "Nd"(port));
}
static inline uint8_t inb(uint16_t port) {
  volatile uint8_t value;
  asm (
    "inb %1, %0"
  : "=a"(value) : "Nd"(port));
  return value;
}
static inline void outw(uint16_t port, uint16_t value) {
  asm (
    "outw %0, %1"
  : : "a"(value), "Nd"(port));
}
static inline uint16_t inw(uint16_t port) {
  volatile uint16_t value;
  asm (
    "inw %1, %0"
  : "=a"(value) : "Nd"(port));
  return value;
}
static inline void outd(uint16_t port, uint32_t value) {
  asm (
    "outl %0, %1"
  : : "a"(value), "Nd"(port));
}
static inline uint32_t ind(uint16_t port) {
  volatile uint32_t value;
  asm (
    "inl %1, %0"
  : "=a"(value) : "Nd"(port));
  return value;
}

void memcpy(void *to, const void *from, uint32_t n);
void fmcpy(void *to, const struct drive *d, file_id_t f, uint32_t from, uint32_t n);
void mfcpy(const struct drive *d, file_id_t f, uint32_t to, const void *from, uint32_t n);

uint8_t u32_dec(uint32_t n, char *b);
uint8_t u16_dec(uint16_t n, char *b);
uint8_t u8_dec(uint8_t n, char *b);
void u32_hex(uint32_t n, char *b);
void u16_hex(uint16_t n, char *b);
void u8_hex(uint8_t n, char *b);

#endif
