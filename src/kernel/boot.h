#ifndef BOOT_H
#define BOOT_H

#include <stdint.h>

enum {
  BIS_PCI = 0x80,
  BIS_PAE = 0x40
};

enum {
  PHC_CS_M1 = 0x01,
  PHC_CS_M2 = 0x02,
  PHC_SC_M1 = 0x10,
  PHC_SC_M2 = 0x20
};

#define BOOT_INFO \
  ((struct { \
    uint8_t support_flags; \
    uint8_t pci_hw_char; \
    uint8_t pci_minor_bcd; \
    uint8_t pci_major_bcd; \
    uint8_t last_pci_bus; \
  } __attribute__ ((__packed__)) *)0x4000)

#endif