#ifndef FAT_H
#define FAT_H

#include <stdbool.h>
#include "drive.h"

void init_fat();

bool try_fat_init_drive(struct drive *d);

void fat_ready_shutdown();

#endif