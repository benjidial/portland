#ifndef LOG_H
#define LOG_H

#include <stdarg.h>

enum log_level {
  LOG_USER,
  LOG_INFO,
  LOG_DUMP,
  LOG_WARN,
  LOG_ERROR,
  LOG_PANIC
};

void init_log();
void logf(enum log_level level, const char *format, ...);

#endif
