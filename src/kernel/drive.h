#ifndef DRIVE_H
#define DRIVE_H

#include <stdbool.h>
#include <stdint.h>

typedef uint8_t file_id_t;

typedef uint8_t fs_id_t;
typedef uint8_t drive_id_t;

#define MAX_DRIVES 256
#define DCI_NAME_LEN 100

struct directory_content_info {
  char name[DCI_NAME_LEN];
  uint32_t size;
  bool is_dir;
  uint8_t pad[23];
} __attribute__ ((packed));

struct drive {
  char *drive_type;
  char *fs_type;
  char *mapped_to;
  uint32_t uid;

  uint32_t (*read_sectors) (const struct drive *d, uint32_t start, uint32_t count, void *buffer);
  uint32_t (*write_sectors)(const struct drive *d, uint32_t start, uint32_t count, const void *buffer);
  void     (*ready)        (const struct drive *d);
  void     (*done)         (const struct drive *d);
  uint32_t n_sectors;
  drive_id_t drive_id;

  file_id_t (*get_file)        (const struct drive *d, const char *path);
  void      (*free_file)       (const struct drive *d, file_id_t fid);
  void      (*load_sector)     (const struct drive *d, file_id_t fid, uint32_t sector, void *at);
  void      (*save_sector)     (const struct drive *d, file_id_t fid, uint32_t sector, const void *from);
  bool      (*is_writable)     (const struct drive *d, file_id_t fid);
  uint32_t  (*get_file_length) (const struct drive *d, file_id_t fid);
  void      (*set_file_length) (const struct drive *d, file_id_t fid, uint32_t new_len);
  uint32_t  (*enumerate_dir)   (const struct drive *d, const char *path, struct directory_content_info *info, uint32_t max);
  uint32_t  (*n_dir_entries)   (const struct drive *d, const char *path);
  uint32_t  (*get_free_sectors)(const struct drive *d);
  fs_id_t fs_id;
};

void map_path(const char *full, struct drive **d, const char **path);

extern uint8_t n_drives;
extern struct drive drives[MAX_DRIVES];

void init_drives();
void commit_drive(struct drive data);
void map_drives();

extern bool ignore_already_open;

#endif