#ifndef VBE_H
#define VBE_H

#include <stdint.h>

struct segoff {
  uint16_t offset;
  uint16_t segment;
} __attribute__ ((__packed__));

#define RM_PTR(type, segoff) ((type *)((segoff).segment * 16 + (segoff).offset))

enum vbe_capabilities {
  VBE_CAP_SWITCHABLE_DAC = 0x1,
  VBE_CAP_NON_VGA        = 0x2,
  VBE_CAP_WEIRD_RAMDAC   = 0x4
};

#define VBE_INFO                      \
  ((struct {                          \
      uint8_t signature[4];           \
      uint8_t minor_version;          \
      uint8_t major_version;          \
      struct segoff oem_name;         \
      uint32_t capabilities;          \
      struct segoff mode_list;        \
      /*in units of 64kiB*/           \
      uint16_t total_memory;          \
      uint16_t version_rev;           \
      struct segoff vendor_name;      \
      struct segoff product_name;     \
      struct segoff product_rev_name; \
    } __attribute__ ((__packed__)) *)0x4200)

#define VBE_MODE_INFO             \
  ((struct {                      \
      uint16_t attribs;           \
      uint8_t wina_attribs;       \
      uint8_t winb_attribs;       \
      uint16_t win_gran;          \
      uint16_t win_size;          \
      uint16_t wina_seg;          \
      uint16_t winb_seg;          \
      struct segoff win_func;     \
      uint16_t pitch;/*in bytes*/ \
      uint16_t width;             \
      uint16_t height;            \
      uint8_t char_width;         \
      uint8_t char_height;        \
      uint8_t n_planes;           \
      uint8_t bpp;                \
      uint8_t n_banks;            \
      uint8_t mem_model;          \
      uint8_t bank_size;/*in kiB*/\
      uint8_t image_pages;        \
      uint8_t reserved;           \
      uint8_t red_len;            \
      uint8_t red_off;            \
      uint8_t green_len;          \
      uint8_t green_off;          \
      uint8_t blue_len;           \
      uint8_t blue_off;           \
      uint8_t alpha_len;          \
      uint8_t alpha_off;          \
      uint8_t color_attribs;      \
      void *frame_buf;            \
      void *off_screen;           \
      uint16_t off_screen_length; \
    } __attribute__ ((__packed__)) *)0x4400)

#endif