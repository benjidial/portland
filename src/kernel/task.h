#ifndef TASK_H
#define TASK_H

#include <stdbool.h>
#include <stdint.h>

#define TASK_NAME_LEN 86
#define MAX_WAITS 16
#define MAX_TASKS 64

struct wait {
  enum {
    NONE,
    PROCESS_END,
    WINDOW_ACTION,
    IPC_SENT,
    IPC_SENT_ANY,
    IPC_READ
  } mode;
  union {
    struct task_state *task;
  };
};

struct task_state {
  uint32_t ret_addr;
  void *page_directory;

  uint32_t ebx;
  uint32_t ecx;
  uint32_t edx;
  uint32_t esi;
  uint32_t edi;
  uint32_t ebp;
  uint32_t esp;

  uint32_t stack_bottom;

  struct wait waits[MAX_WAITS];
  bool waiting;

  char name[TASK_NAME_LEN + 1];
} __attribute__ ((packed));

void add_wait(struct wait wait);
void unwait_any(struct wait wait);
void unwait(struct task_state *task, struct wait wait);

extern struct task_state tasks[];
extern struct task_state *active_task;

void init_tasks();

//puts the handle into ecx
//returns null on full
struct task_state *new_task();
void advance_active_task();

void delete_task(struct task_state *state);

uint32_t ipc_send(uint32_t reader_handle, uint32_t count, const void *buffer);
uint32_t ipc_read(uint32_t sender_handle, uint32_t count, void *buffer);

uint32_t find_unread_ipc() __attribute__ ((pure));

#endif