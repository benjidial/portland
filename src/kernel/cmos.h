#ifndef CMOS_H
#define CMOS_H

#include <stdint.h>

struct rtc_time {
  uint8_t seconds;
  uint8_t minutes;
  uint8_t hours;
  uint8_t day_of_week;
  uint8_t day_of_month;
  uint8_t month;
  uint8_t year;
};

struct rtc_time get_rtc_time();
void cmos_init();

#endif