#include <stdint.h>

#include "log.h"

__attribute__ ((noreturn))
void panic(const char *filename, uint32_t line, const char *message) {
  logf(LOG_PANIC, "[%s:%d] %s", filename, line, message);
  logf(LOG_INFO, "Halting");
  while (1)
    asm volatile ("hlt");
}
