#ifndef SETTINGS_H
#define SETTINGS_H

#include "window.h"

#include <stdbool.h>
#include <stdint.h>

void init_settings();
void close_settings();

//lengths do not include null terminator. if setting value is too long, it is cropped.
bool try_get_sz_setting(const char *name, char *out, uint32_t max_len, uint32_t *len_out);

bool try_get_color_setting(const char *name, struct pixel *out);

#endif