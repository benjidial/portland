#ifndef WINDOW_H
#define WINDOW_H

#include <stdint.h>

#include <winact.h>

#include "task.h"

struct window;

struct pixel {
  uint8_t r;
  uint8_t g;
  uint8_t b;
  uint8_t pad;
} __attribute__ ((__packed__));

void init_win();

struct window *new_window(uint16_t width, uint16_t height, const struct pixel *pixel_buffer);
void del_window(struct window *w);

void resize_window(struct window *w, uint16_t width, uint16_t height, const struct pixel *pixel_buffer);
void reassign_pixel_buffer(struct window *w, const struct pixel *pixel_buffer);
void push_window_paint(const struct window *w);
struct window_action next_window_action(struct window *w);
void wait_window_action();

void on_action(struct window_action packet);

void delete_any_windows_from(struct task_state *tstate);

void show_shutdown();

void move_mouse_by(int16_t y, int16_t x);
void mouse_button(enum mouse_button which, bool up);

void window_wants_mouse_movements(struct window *w);

#endif
