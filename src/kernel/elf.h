#ifndef ELF_H
#define ELF_H

#include <stdint.h>
#include "drive.h"

uint32_t try_elf_run(const struct drive *d, const char *path, const char *pass_old_vma, uint32_t io_handle);

#endif