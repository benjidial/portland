#include "drive.h"
#include "panic.h"
#include "pmap.h"
#include "util.h"
#include "fat.h"
#include "log.h"

uint8_t n_drives;
struct drive drives[256];

bool ignore_already_open = false;

void init_drives() {
  n_drives = 0;
}

__attribute__ ((const))
static file_id_t unknown_get_file(const struct drive *d, const char *path) {
  return 0;
}

__attribute__ ((const))
static uint32_t unknown_get_free_sectors(const struct drive *d) {
  return 0;
}

__attribute__ ((const))
static uint32_t unknown_enumerate_dir(const struct drive *d, const char *path, struct directory_content_info *info, uint32_t max) {
  return 0;
}

__attribute__ ((const))
static uint32_t unknown_n_dir_entries(const struct drive *d, const char *path) {
  return 0;
}

static void unknown_no_call(const struct drive *d) {
  logf(LOG_ERROR, "file-level operation called on drive %u (%s), which is an unknown file system", d - drives + 1, d->fs_type);
}

static inline void determine_fs(struct drive *d) {
  if (try_fat_init_drive(d))
    return;

  d->mapped_to = 0;
  d->fs_type = "Unknown";
  d->get_file = &unknown_get_file;
  d->free_file = &unknown_no_call;
  d->load_sector = &unknown_no_call;
  d->save_sector = &unknown_no_call;
  d->is_writable = &unknown_no_call;
  d->get_file_length = &unknown_no_call;
  d->set_file_length = &unknown_no_call;
  d->enumerate_dir = &unknown_enumerate_dir;
  d->n_dir_entries = &unknown_n_dir_entries;
  d->get_free_sectors = &unknown_get_free_sectors;
}

//drive should be ready before this.
//determine_fs and its children
//do not need to make ready or done
void commit_drive(struct drive data) {
  determine_fs(&data);
  data.mapped_to = 0;
  drives[n_drives] = data;
  data.done(drives + n_drives);
  ++n_drives;
}

void map_path(const char *full, struct drive **d, const char **path) {
  *d = 0;
  *path = 0;

  switch (full[0]) {
  case '/':
    for (struct drive *i = drives; i < drives + n_drives; ++i) {
      if (!i->mapped_to)
        continue;
      uint32_t j = 0;
      while (1) {
        if (!i->mapped_to[j]) {
          if (!*path || (j >= *path - full)) {
            *path = full + j + 1;
            *d = i;
          }
          break;
        }
        else if (i->mapped_to[j] != full[j + 1])
          break;
        ++j;
      }
    }
    return;

    uint32_t id;
  case ':':
    id = 0;
    for (uint32_t i = 1; i < 9; ++i) {
      id *= 16;
      const char c = full[i];
      if ((c >= '0') && (c <= '9'))
        id += c - '0';
      else if ((c >= 'a') && (c <= 'f'))
        id += c - 'a' + 10;
      else
        return;
    }
    if (full[9] != ':')
      return;
    for (struct drive *i = drives; i < drives + n_drives; ++i)
      if (i->uid == id) {
        *d = i;
        *path = full + 10;
        return;
      }
  }
}

void map_drives() {
  for (struct drive *d = drives; d < drives + n_drives; ++d) {
    file_id_t fid = d->get_file(d, "_fstab");
    if (!fid)
      continue;

    const uint32_t len = d->get_file_length(d, fid);
    void *fstab = allocate_kernel_pages(len / 512 + 1);
    fmcpy(fstab, d, fid, 0, len);
    d->free_file(d, fid);
    const uint32_t n_entries = *(uint32_t *)fstab;

    uint32_t next_id = *(uint32_t *)(fstab + 4);
    fstab += 8;
    for (uint32_t i = 0; i < n_entries; ++i) {
      for (struct drive *dd = drives; dd < drives + n_drives; ++dd)
        if (dd->uid == next_id)
          dd->mapped_to = fstab + 4;
      const uint32_t plen = *(uint32_t *)fstab;
      next_id = *(uint32_t *)(fstab + 4 + plen);
      *(char *)(fstab + 4 + plen) = '\0';
      fstab += 8 + plen;
    }
  }
}