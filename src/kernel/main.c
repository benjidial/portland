#include "settings.h"
#include "paging.h"
#include "serial.h"
#include "window.h"
#include "panic.h"
#include "boot.h"
#include "cmos.h"
#include "pmap.h"
#include "task.h"
#include "util.h"
#include "elf.h"
#include "fat.h"
#include "ide.h"
#include "idt.h"
#include "kbd.h"
#include "log.h"
#include "pci.h"
#include "vbe.h"

#include <stdint.h>

void _start_user_mode() __attribute__ ((noreturn));

__attribute__ ((noreturn))
void main() {
  cmos_init();
  init_pagemap();
  init_paging();
  init_tasks();
  init_serial();
  pci_init();

  init_fat();
  //other fs drivers

  init_drives();

  init_ide();
  //other drive drivers

  ignore_already_open = true;

  map_drives();

  init_log();
  init_settings();

  init_kbd();
  init_idt();
  init_win();

  close_settings();

  logf(LOG_INFO, "Kernel initialization done.");
  logf(LOG_INFO, "Available kernel memory: %dk", kernel_pages_left * 4);
  logf(LOG_INFO, "Available user memory: %dk", user_pages_left * 4);

  logf(LOG_INFO, "");

  logf(LOG_INFO, "PCI devices:");
  for (uint16_t i = 0; i < n_pci_devices; ++i) {
    const struct pci_device *dev = nth_pci_device(i);
    logf(LOG_INFO, "  %hw:%hw (%hb:%hb)", dev->id_vendor, dev->id_device, dev->class, dev->subclass);
  }

  logf(LOG_INFO, "");

  logf(LOG_INFO, "Drives:");
  for (struct drive *d = drives; d < drives + n_drives; ++d) {
    const uint32_t free = d->get_free_sectors(d);
    logf(LOG_INFO, "  :%h:", d->uid);
    logf(LOG_INFO, "    %s (%d%skB free)", d->fs_type, free / 2, free % 2 ? ".5" : "");
    logf(LOG_INFO, "    on %s (%d%skB)", d->drive_type, d->n_sectors / 2, d->n_sectors % 2 ? ".5" : "");
    if (d->mapped_to)
      logf(LOG_INFO, "    mapped to /%s", d->mapped_to);
    else
      logf(LOG_INFO, "    not mapped");
  }

  logf(LOG_INFO, "");

  logf(LOG_INFO, "VBE info:");
  logf(LOG_INFO, "  Implemention: %s", RM_PTR(char, VBE_INFO->oem_name));
  logf(LOG_INFO, "  Video memory: %dk", VBE_INFO->total_memory * 64);
  logf(LOG_INFO, "  Standard:     %d.%d.%d", VBE_INFO->major_version, VBE_INFO->minor_version, VBE_INFO->version_rev);
  logf(LOG_INFO, "  Vendor:  %s", RM_PTR(char, VBE_INFO->vendor_name));
  logf(LOG_INFO, "  Product: %s", RM_PTR(char, VBE_INFO->product_name));
  logf(LOG_INFO, "  Version: %s", RM_PTR(char, VBE_INFO->product_rev_name));

  logf(LOG_INFO, "");

  #define MASK(offset, length) ((~((1 << offset) - 1)) - (offset + length == 32 ? 0 : (~((1 << (offset + length)) - 1))))
  logf(LOG_INFO, "Active video mode:");
  logf(LOG_INFO, "  Resolution: %dx%dx%d", VBE_MODE_INFO->width, VBE_MODE_INFO->height, VBE_MODE_INFO->bpp);
  logf(LOG_INFO, "  Red mask:   0x%h", MASK(VBE_MODE_INFO->red_off,   VBE_MODE_INFO->red_len));
  logf(LOG_INFO, "  Green mask: 0x%h", MASK(VBE_MODE_INFO->green_off, VBE_MODE_INFO->green_len));
  logf(LOG_INFO, "  Blue mask:  0x%h", MASK(VBE_MODE_INFO->blue_off,  VBE_MODE_INFO->blue_len));
  logf(LOG_INFO, "  Alpha mask: 0x%h", MASK(VBE_MODE_INFO->alpha_off, VBE_MODE_INFO->alpha_len));
  logf(LOG_INFO, "  Framebuffer address: 0x%h", VBE_MODE_INFO->frame_buf);

  logf(LOG_INFO, "");

  logf(LOG_INFO, "Loading init program.");

  if (!try_elf_run(drives, "bin/init", "", 0))
    PANIC("Failed to load init program.");

  ignore_already_open = false;

  logf(LOG_INFO, "Switching to init task.");

  _start_user_mode();
}