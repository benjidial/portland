#include "paging.h"
#include "window.h"
#include "fat.h"
#include "log.h"

__attribute__ ((noreturn))
void shutdown() {
  logf(LOG_INFO, "Shutting down...");

  asm ("cli");
  switch_to_kernel_cr3();

  fat_ready_shutdown();

  logf(LOG_INFO, "Finished getting ready for shutdown.");

  show_shutdown();
  while (1)
    asm ("hlt");
}