#ifndef PAGEMAP_H
#define PAGEMAP_H

#include <stdint.h>

extern uint32_t kernel_pages_left;
extern uint32_t user_pages_left;
extern uint32_t max_user_pages;
extern uint32_t max_kernel_pages;

void init_pagemap();

void *allocate_kernel_pages(uint32_t n) __attribute__ ((malloc));
void *allocate_user_pages(uint32_t n) __attribute__ ((malloc));
void free_pages(const void *ptr, uint32_t n);

#endif
