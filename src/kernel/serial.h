#ifndef SERIAL_H
#define SERIAL_H

#include <stdint.h>

enum serial_port {
  COM1,
  COM2,
  COM3,
  COM4
};

void reset_error(enum serial_port n);
void init_serial();

void sout(enum serial_port n, uint8_t b);
uint8_t sin(enum serial_port n);

#endif