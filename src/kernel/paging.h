#include <stdint.h>
#include <stdbool.h>

void init_paging();

void free_task_pd(void *pd);
void *new_task_pd();
void *pd_user_allocate(void *pd, uint32_t vma, uint32_t pages, bool writable);
void *pd_user_allocate_anywhere_writable(void *pd, uint32_t pages);
void user_allocate_anywhere_readonly_together(void *pd, uint32_t pages, void **vma_out, void **pma_out);

void *vma_to_pma(void *pd, const void *vma) __attribute__ ((pure));

void switch_to_kernel_cr3();
void switch_to_task_cr3();

bool pd_is_mapped(const void *pd, uint32_t vma) __attribute__ ((pure));