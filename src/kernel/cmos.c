#include "cmos.h"
#include "util.h"

static inline uint8_t read_cmos(uint8_t reg) {
  outb(0x0070, reg);
  for (uint32_t i = 0; i < 1000000; ++i)
    ;//spin
  return inb(0x0071);
}

static inline void write_cmos(uint8_t reg, uint8_t value) {
  outb(0x0070, reg);
  for (uint32_t i = 0; i < 1000000; ++i)
    ;//spin
  outb(0x0071, value);
}

void cmos_init() {
  write_cmos(0x0b, read_cmos(0x0b) | 0x06);
}

//The idea of reading until you get the same value twice, and checking the status register
//  is from the OSDev Wiki page at <https://wiki.osdev.org/CMOS#The_Real-Time_Clock>.
struct rtc_time get_rtc_time() {
  uint8_t prev_sec = -1;
  struct rtc_time ret;
get_sec:
  while (read_cmos(0x0a) & 0x80)
    ;//spin while rtc is being updated
  ret.seconds      = read_cmos(0x00);
  if (ret.seconds != prev_sec) {
    prev_sec = ret.seconds;
    goto get_sec;
  }
  ret.minutes      = read_cmos(0x02);
  ret.hours        = read_cmos(0x04);
  ret.day_of_week  = read_cmos(0x06);
  ret.day_of_month = read_cmos(0x07);
  ret.month        = read_cmos(0x08);
  ret.year         = read_cmos(0x09);
  return ret;
}