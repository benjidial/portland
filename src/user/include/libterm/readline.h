#ifndef LIBTERM_READLINE_H
#define LIBTERM_READLINE_H

#include <stdint.h>

struct history;

//returns zero if memory allocation failed or if max_entries was zero or one
struct history *new_history(uint32_t max_entries);

//returns length of string without null terminator
//max_length doesn't include null terminator
//pass null pointer in hist for no history
uint32_t read_line(char *sz, uint32_t max_length, const char *prompt, struct history *hist);

#endif