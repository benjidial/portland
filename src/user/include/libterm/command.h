#ifndef LIBTERM_COMMAND_H
#define LIBTERM_COMMAND_H

#include <libterm/terminal.h>

#include <knob/ipc.h>

#include <keypack.h>

#include <stdbool.h>
#include <stdint.h>

//set to stdio task by default
extern _task_handle_t term_task;

struct terminal_command {
  enum {
    SET_DIMENSIONS,
    GET_DIMENSIONS,
    PAINT,
    CLEAR,
    SET_COLOR,
    SET_CURSOR,
    CURSOR_LEFT,
    CURSOR_RIGHT,
    CURSOR_UP,
    CURSOR_DOWN,
    ADD_CHAR,
    ADD_SN,
    ADD_SN_NO_WORDWRAP,
    GET_KEY
  } kind;

  union {
    struct {
      uint32_t y;
      uint32_t x;
    } as_coords;
    struct {
      _pixel_t fg;
      _pixel_t bg;
    } as_color;
    char as_char;
    uint32_t as_uint;
  };
} __attribute__ ((__packed__));

union terminal_response {
  struct {
    uint32_t y;
    uint32_t x;
  } as_coords;
  struct key_packet as_key;
} __attribute__ ((__packed__));

//returns false if terminal has died
static inline bool try_send_command(struct terminal_command *cmd) {
  return try_send_ipc(term_task, cmd, sizeof(struct terminal_command))
                                   == sizeof(struct terminal_command);
}

//returns false if terminal has died
static inline bool try_get_response(union terminal_response *rs) {
  return try_read_ipc(term_task, rs, sizeof(union terminal_response))
                                  == sizeof(union terminal_response);
}

#endif