#ifndef LIBTERM_TERMINAL_H
#define LIBTERM_TERMINAL_H

#ifdef __cplusplus
extern "C" {
#endif

#include <pland/syscall.h>

#include <stdarg.h>
#include <stdint.h>

void term_set_dimensions(uint32_t width, uint32_t height);
void term_get_dimensions(uint32_t *width, uint32_t *height);

void term_paint();
void term_clear();

void term_set_color(_pixel_t fg, _pixel_t bg);
void term_set_cursor(uint32_t new_y, uint32_t new_x);

void term_cursor_left();
void term_cursor_right();
void term_cursor_up();
void term_cursor_down();

void term_add_char(char ch);
void term_add_sn_no_ww(const char *s, uint32_t n);
void term_add_sz_no_ww(const char *sz);
void term_add_sz(const char *sz);

void term_addf_no_ww_v(const char *fmt, va_list args);
void term_addf_no_ww(const char *fmt, ...);
void term_addf_v(const char *fmt, va_list args);
void term_addf(const char *fmt, ...);

struct key_packet term_get_key_blocking();

#ifdef __cplusplus
}
#endif

#endif