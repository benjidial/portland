#ifndef KNOB_IPC_H
#define KNOB_IPC_H

#include <pland/syscall.h>

//blocking, returns early if other process is dead.
//return value is number of bytes written.
uint32_t try_send_ipc(_task_handle_t to, const void *buffer, uint32_t size);

//blocking, returns early if other process is dead.
//return value is number of bytes read.
uint32_t try_read_ipc(_task_handle_t from, void *buffer, uint32_t size);

void flush_ipc(_task_handle_t from);

#endif