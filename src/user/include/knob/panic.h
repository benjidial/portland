#ifndef KNOB_PANIC_H
#define KNOB_PANIC_H

#include <stdint.h>

#define PANIC(msg) panic(__FILE__, __LINE__, msg)
void panic(const char *filename, uint32_t line, const char *message) __attribute__ ((noreturn));

#endif