#ifndef KNOB_HEAP_H
#define KNOB_HEAP_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

void *get_block(uint32_t bytes) __attribute__ ((malloc));
void free_block(const void *block);

#ifdef __cplusplus
}
#endif

#endif