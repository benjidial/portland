#ifndef KNOB_FILE_H
#define KNOB_FILE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <pland/syscall.h>

struct file;

struct file *open_file(const char *path);
void close_file(struct file *f);

uint32_t read_from_file(struct file *f, uint32_t max, void *buf);
uint32_t write_to_file(struct file *f, uint32_t max, const void *buf);
//return value and max_length don't include null terminator
uint32_t read_line_from_file(struct file *f, char *sz, uint32_t max_length);
uint32_t seek_file_to(struct file *f, uint32_t to);
int32_t seek_file_by(struct file *f, int32_t by);

uint32_t get_file_pos(struct file *f) __attribute__ ((pure));
uint32_t file_size(struct file *f) __attribute__ ((pure));
void trunc_file(struct file *f);

//return value must be manually freed, unless it is a null pointer
_dir_info_entry_t *get_directory_info(const char *path, uint32_t *count_out);

#ifdef __cplusplus
}
#endif

#endif