#ifndef KNOB_FORMAT_H
#define KNOB_FORMAT_H

#ifdef __cplusplus
extern "C" {
#endif

#include <knob/heap.h>

#include <stdint.h>
#include <stdarg.h>

//allocates new memory
char *format_v(const char *fmt, va_list args);

//allocates new memory
char *format(const char *fmt, ...);

void syslogf_v(const char *fmt, va_list args);

void syslogf(const char *fmt, ...);

//reads a unsigned decimal terminated by either null or whitespace
//returns length of string plus length of whitespace
//returns 0 on failure
uint32_t try_swtou(const char *from, uint32_t *i_out);

//reads a hexadecimal terminated by either null or whitespace
//returns length of string plus length of whitespace
//returns 0 on failure
uint32_t try_swtoh(const char *from, uint32_t *i_out);

#ifdef __cplusplus
}
#endif

#endif