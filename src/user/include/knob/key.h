#ifndef KNOB_KEY_H
#define KNOB_KEY_H

#ifdef __cplusplus
extern "C" {
#endif

#include <keypack.h>

char key_to_char(struct key_packet kp) __attribute__ ((const));
bool match_side_agnostic(struct key_packet a, struct key_packet b) __attribute__ ((const));

#ifdef __cplusplus
}
#endif

#endif