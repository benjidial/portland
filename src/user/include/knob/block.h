#ifndef KNOB_BLOCK_H
#define KNOB_BLOCK_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>

void blockcpy(void *to, const void *from, uint32_t size);
bool blockequ(const void *a, const void *b, uint32_t size) __attribute__ ((__pure__));

//returns length without null-terminator
uint32_t strcpy(char *to, const char *from);

//allocates new memory
char *strdup(const char *from);
char *strndup(const char *from, uint32_t new_len);

//without null-terminator
uint32_t strlen(const char *str) __attribute__ ((pure));

bool strequ(const char *a, const char *b) __attribute__ ((pure));

//if str has length == len, nothing is done
//if str has length  < len, it is right-padded with spaces
//if str has length  > len, the end is replaced with " ..."
//this replacement happens in place, with no memory allocation
void str_trunc_fill(char *str, uint32_t len);

//returns first index of any character of delims or the null terminator in str
uint32_t str_find_any(const char *str, const char *delims) __attribute__ ((pure));

#ifdef __cplusplus
}
#endif

#endif