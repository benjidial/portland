#include <stdint.h>

struct time {
  uint16_t year;//ad
  uint8_t month;//jan = 1
  uint8_t day;
  uint8_t hour;//24-hour, midnight = 0
  uint8_t minute;
  uint8_t second;
  uint32_t timestamp;
};

struct time get_time();