#ifndef KNOB_TASK_H
#define KNOB_TASK_H

#include <pland/syscall.h>

#include <stdint.h>
#include <stdbool.h>

uint32_t run_command(const char *path, _task_handle_t stdio_task);
bool try_run_command_blocking(const char *path, _task_handle_t stdio_task);

#endif