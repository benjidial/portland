#ifndef LIBFONT_FONTS_H
#define LIBFONT_FONTS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <pland/syscall.h>

#include <stdbool.h>
#include <stdint.h>

struct font_info {
  uint32_t space_width;
  uint32_t space_height;
  uint32_t char_width;
  uint32_t char_height;
  bool *bitmaps[256];//left to right then top to bottom
                     //null pointer for unsupported character
                     //unsupported characters drawn as bitmaps[0]
};

struct font_info *get_font(const char *name);

//pitch is in pixels
void put_char(const struct font_info *font, char ch, _pixel_t *pb_ptr, uint32_t pb_pitch, _pixel_t bg, _pixel_t fg);

//pitch is in pixels
void put_char_no_bg(const struct font_info *font, char ch, _pixel_t *pb_ptr, uint32_t pb_pitch, _pixel_t fg);

#ifdef __cplusplus
}
#endif

#endif