#ifndef STRUCTS_DUPLE_H
#define STRUCTS_DUPLE_H

template<class at, class bt>
class duple {
public:
  duple() : a(), b() {}
  duple(at a, bt b)
    : a(a), b(b) {}
  at a;
  bt b;
};

#endif