#ifndef STRUCTS_MAP_H
#define STRUCTS_MAP_H

#include <structs/alist.h>

template<class key, class value, bool (*equals)(key, key) = &key::operator==>
class map {
public:
  alist<key> keys;
  alist<value> values;

  map(uint32_t default_size=10, uint32_t expand_by=10)
    : keys(default_size, expand_by), values(default_size, expand_by) {}

  void add_pair(key k, value v) {
    for (key *i = keys.buf; i < keys.buf + keys.n_entries; ++i)
      if (equals(k, *i)) {
        values.buf[i - keys.buf] = v;
        return;
      }
    keys.add_back(k);
    values.add_back(v);
  }

  __attribute__ ((pure))
  value transform(key k, value fallback=value()) {
    for (key *i = keys.buf; i < keys.buf + keys.n_entries; ++i)
      if (equals(k, *i))
        return values.buf[i - keys.buf];
    return fallback;
  }
};

#endif