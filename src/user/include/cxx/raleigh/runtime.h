#ifndef RALEIGH_RUNTIME_H
#define RALEIGH_RUNTIME_H

#include <raleigh/window.h>
#include <structs/dllist.h>

namespace raleigh {
  void start_runtime() __attribute__ ((noreturn));
  extern dllist<window &> open_windows;
  extern dllist<window &> to_be_deleted;
}

#endif