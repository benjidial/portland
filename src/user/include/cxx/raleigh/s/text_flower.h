#ifndef RALEIGH_S_TEXT_FLOWER_H
#define RALEIGH_S_TEXT_FLOWER_H

#include <libfont/fonts.h>
#include <structs/alist.h>

namespace raleigh {
  class text_flower {
  public:
    //max_rows or cols 0 means no limit
    //s is not copied
    text_flower(const char *s, uint32_t cols=50, uint32_t max_rows=0);

    uint32_t get_n_lines() __attribute__ ((pure));
    //not a copy
    char *get_nth_line(uint32_t n) __attribute__ ((pure));
    uint32_t get_line_offset(uint32_t n) __attribute__ ((pure));

    void draw_text(_pixel_t *start, uint32_t pitch, const struct font_info *fi, _pixel_t color);
    void flow_text();

    //call flow_text after any changes
    const char *s;

  private:
    alist<char *> lines;
    alist<uint32_t> offsets;

    uint32_t max_rows;
    uint32_t cols;

    //for use inside flow_text
    const char *on_char;
    const char *line_start;
    uint32_t row_len;
    void push_line();
  };
}

#endif