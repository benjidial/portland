#ifndef RALEIGH_UTIL_H
#define RALEIGH_UTIL_H

#include <stdint.h>

#define RGB(R, G, B) ((_pixel_t){.r = 0x##R, .g = 0x##G, .b = 0x##B})

#define RALEIGH_FG RGB(00, 00, 00)
#define RALEIGH_BG RGB(bf, bf, bf)

namespace raleigh {
  struct coord {
    uint32_t x;
    uint32_t y;
    coord(uint32_t x, uint32_t y);
    coord();
  };

  void show_error_and_quitf(const char *fmt, ...) __attribute__ ((noreturn));
  void show_error_popup_and_quitf(const char *fmt, ...) __attribute__ ((noreturn));
}

#endif