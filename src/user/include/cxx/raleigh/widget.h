#ifndef RALEIGH_WIDGET_H
#define RALEIGH_WIDGET_H

namespace raleigh {
  class widget;
}

#include <raleigh/window.h>
#include <pland/syscall.h>
#include <raleigh/util.h>

namespace raleigh {
  class widget {
  public:
    //these four are set by window class (or parent widget)
    widget *parent;//set to zero when root widget
    window *w;
    coord window_offset;
    bool next_paint_full;

    //derived classes should not set this outside of the initializer
    //instead, they should call widget::set_size(coord)
    coord size;

    //fewest steps up that a widget can be redrawn without needing its parents
    //if a widget is opaque, it will set this to a pointer to itself, and then call
    //  notify_has_opaque_parent on any children, passing itself as an argument.
    //in notify_has_opaque_parent's handler, it should set this if it isn't already set,
    //  and then call notify_has_opaque_parent on any children (with the opaque parent).
    widget *closest_opaque;

    //these are called by window class (or parent widgets)
    virtual void notify_window_change();
    virtual void paint(_pixel_t *pixbuf, uint32_t pitch) = 0;
    virtual void handle_click(coord window_coords, enum mouse_packet::mouse_button click_type, bool up);
    virtual void notify_has_opaque_parent(widget *parent) = 0;
    virtual void handle_key(struct key_packet kp);
    virtual void on_focus();
    virtual void on_unfocus();
    virtual void on_mouse_move(coord window_coords);
    //this next one is not to be called by child widgets
    //they should call window::notify_widget_size_change(widget &), which will call this if necessary
    virtual void notify_child_size_change(widget &child, coord old_size);

  protected:
    widget();
    void set_size(coord to);
  };
}

#endif