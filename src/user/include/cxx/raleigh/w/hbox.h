#ifndef RALEIGH_W_HBOX_H
#define RALEIGH_W_HBOX_H

#include <raleigh/w/multicontainer.h>

namespace raleigh {
  class hbox : public multicontainer {
  public:
    //do not modify this list afterward
    hbox(dllist<widget &> widgets);

  private:
    coord determine_size() override;
    void set_child_offsets() override;
  };
}

#endif