#ifndef RALEIGH_W_PADDING_H
#define RALEIGH_W_PADDING_H

#include <raleigh/widget.h>

namespace raleigh {
  class padding : public widget {
  public:
    padding(widget &inner, uint32_t pad_by);

    void notify_window_change() override;
    void paint(_pixel_t *pixbuf, uint32_t pitch) override;
    void handle_click(coord window_coords, enum mouse_packet::mouse_button click_type, bool up) override;
    void notify_has_opaque_parent(widget *parent) override;
    void notify_child_size_change(widget &child, coord old_size) override;
  private:
    widget &inner;
    uint32_t pad_by;
  };
}

#endif