#ifndef RALEIGH_W_COLORPICKER_H
#define RALEIGH_W_COLORPICKER_H

#include <raleigh/widget.h>

namespace raleigh {
  class colorpicker : public widget {
  public:
    colorpicker(_pixel_t default_color=RGB(20, 70, 30), uint8_t resolution=4);
    _pixel_t get_picked_color() __attribute__ ((pure));
    void set_picked_color(_pixel_t c);

    void paint(_pixel_t *pixbuf, uint32_t pitch) override;
    void handle_click(coord window_coords, enum mouse_packet::mouse_button click_type, bool up) override;
    void notify_has_opaque_parent(widget *parent) override;
    void on_mouse_move(coord window_coords) override;

  private:
    _pixel_t picked_color;
    uint8_t resolution;
    uint8_t inv_res;

    enum {R, G, B} selected;
  };
}

#endif