#ifndef RALEIGH_W_MULTICONTAINER_H
#define RALEIGH_W_MULTICONTAINER_H

#include <raleigh/widget.h>
#include <structs/dllist.h>

namespace raleigh {
  class multicontainer : public widget {
  public:
    void add_end(widget &w);
    void add_start(widget &w);

    void notify_window_change() override;
    void paint(_pixel_t *pixbuf, uint32_t pitch) override;
    void handle_click(coord window_coords, enum mouse_packet::mouse_button click_type, bool up) override;
    void notify_has_opaque_parent(widget *parent) override;
    void notify_child_size_change(widget &from, coord old_size) override;

  protected:
    //do not modify this list afterward
    //set size to determine_size() in derived constructor
    multicontainer(dllist<widget &> widgets);

    virtual coord determine_size() = 0;
    virtual void set_child_offsets() = 0;

    dllist<widget &> widgets;
  };
}

#endif