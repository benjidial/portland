#ifndef RALEIGH_W_ENTRY_H
#define RALEIGH_W_ENTRY_H

#include <raleigh/s/text_flower.h>
#include <raleigh/widget.h>
#include <libfont/fonts.h>

namespace raleigh {
  class entry : public widget {
  public:
    entry(uint32_t rows=25, uint32_t cols=25, const char *default_text="",
          const struct font_info *fi=get_font("fixed-10"), _pixel_t bg=RGB(ff, ff, ff),
          _pixel_t fg=RGB(00, 00, 00), _pixel_t border_color=RGB(00, 00, 00));

    //not a copy
    const char *get_contents() __attribute__ ((pure));
    //s is copied
    void set_contents(const char *s);

    void paint(_pixel_t *pixbuf, uint32_t pitch) override;
    void notify_has_opaque_parent(widget *parent) override;
    void handle_click(coord window_coords, enum mouse_packet::mouse_button click_type, bool up) override;
    void handle_key(struct key_packet kp) override;
    void on_focus() override;
    void on_unfocus() override;

  private:
    const struct font_info *fi;
    _pixel_t border_color;
    _pixel_t fg;
    _pixel_t bg;
    uint32_t rows;
    uint32_t cols;

    void left();
    void right();
    void up();
    void down();

    void check_y();
    void check_x();

    void on_text_change();
    void on_cursor_change();

    alist<char> text_back;
    text_flower flower;

    bool text_change;
    bool cursor_change;

    bool was_cur_before;
    uint32_t old_cur_x;
    uint32_t old_cur_y;

    bool is_cur;
    uint32_t cur_x;
    uint32_t cur_y;
  };
}

#endif