#ifndef RALEIGH_W_LABEL_H
#define RALEIGH_W_LABEL_H

#include <raleigh/s/text_flower.h>
#include <raleigh/widget.h>
#include <libfont/fonts.h>

namespace raleigh {
  class label : public widget {
  public:
    //value's data is copied
    label(const char *value, const char *font="fixed-10",
          uint32_t cols=0, bool bg_transparent=true,
          _pixel_t fg=RALEIGH_FG, _pixel_t bg=RALEIGH_BG);

    ~label();

    //new_value's data is copied
    void change_value(const char *new_value);

    void paint(_pixel_t *pixbuf, uint32_t pitch) override;
    void notify_has_opaque_parent(widget *parent) override;
  private:
    const struct font_info *const fi;
    uint32_t cols;
    bool bg_transparent;
    const _pixel_t fg;
    const _pixel_t bg;

    text_flower tf;

    coord determine_size();
  };
}

#endif