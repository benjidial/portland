#ifndef RALEIGH_W_VBOX_H
#define RALEIGH_W_VBOX_H

#include <raleigh/w/multicontainer.h>

namespace raleigh {
  class vbox : public multicontainer {
  public:
    //do not modify this list afterward
    vbox(dllist<widget &> widgets=dllist<widget &>());

  private:
    coord determine_size() override;
    void set_child_offsets() override;
  };
}

#endif