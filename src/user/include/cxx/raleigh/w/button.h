#ifndef RALEIGH_W_BUTTON_H
#define RALEIGH_W_BUTTON_H

#include <raleigh/widget.h>

namespace raleigh {
  typedef void *button_tag_t;
  class button : public widget {
  public:
    button(widget &inner, void (*on_click)(button_tag_t), button_tag_t tag=0,
      _pixel_t border_color=RGB(00, 00, 00), _pixel_t bg_color=RGB(bf, bf, bf),
      _pixel_t pressed_color=RGB(9f, 9f, 9f));

    void notify_window_change() override;
    void paint(_pixel_t *pixbuf, uint32_t pitch) override;
    void handle_click(coord window_coords, enum mouse_packet::mouse_button click_type, bool up) override;
    void notify_child_size_change(widget &child, coord old_size) override;
    void notify_has_opaque_parent(widget *parent) override;
  private:
    widget &inner;
    void (*on_click)(button_tag_t);
    button_tag_t tag;
    _pixel_t border_color;
    _pixel_t bg_color;
    _pixel_t pressed_color;
    bool is_pressed;
  };
}

#endif