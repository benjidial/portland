#ifndef RALEIGH_WINDOW_H
#define RALEIGH_WINDOW_H

#include <stdint.h>

namespace raleigh {
  class window;
}

#include <raleigh/runtime.h>
#include <raleigh/widget.h>
#include <pland/syscall.h>
#include <raleigh/util.h>
#include <structs/map.h>
#include <knob/key.h>

namespace raleigh {
  typedef void *window_tag_t;
  class window {
    friend void start_runtime();
  public:
    //pass on_close to specify a close handler. if on_close returns false, the window will not be closed.
    window(widget &root, _pixel_t bg_color=RALEIGH_BG, bool (*on_close)(window_tag_t)=0, window_tag_t tag=0);
    void add_keybind(struct key_packet kp, void (*handler)(window_tag_t));

    void notify_needs_paint(widget &from);
    void notify_widget_size_change(widget &from, coord old_size);
    void notify_wants_movements(widget &from, enum mouse_packet::mouse_button while_down);
    void consume_actions();
    void show();
    void focus(widget &w);

  protected:
    widget &root;
  private:
    _window_handle_t handle;
    _pixel_t *pixbuf;
    coord size;
    widget *focussed;
    widget *drag_reciever;
    enum mouse_packet::mouse_button drag_until;
    _pixel_t bg_color;
    bool needs_repaint;
    void paint_full();
    bool (*on_close)(window_tag_t);
    window_tag_t tag;
    map<struct key_packet, void (*)(window_tag_t), &match_side_agnostic> keybinds;
  };
}

#endif