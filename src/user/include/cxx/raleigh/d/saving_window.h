#ifndef RALEIGH_D_SAVING_WINDOW_H
#define RALEIGH_D_SAVING_WINDOW_H

#include <raleigh/w/vbox.h>
#include <raleigh/window.h>

namespace raleigh {
  typedef void *save_tag_t;
  class saving_window : public window {
  public:
    //save_func returns true if saving was successful
    saving_window(bool (*save_func)(save_tag_t), save_tag_t save_tag, widget &root, _pixel_t bg_color=RGB(bf, bf, bf));
    bool is_saved;

    bool (*save_func)(save_tag_t);
    save_tag_t save_tag;
  };
};

#endif