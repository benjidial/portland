#ifndef RALEIGH_D_DIALOG_H
#define RALEIGH_D_DIALOG_H

#include <raleigh/w/vbox.h>
#include <raleigh/window.h>
#include <structs/alist.h>
#include <structs/duple.h>
#include <stdint.h>

namespace raleigh {
  typedef uint32_t diag_result_t;
  enum : diag_result_t {
    NONE = 0,
    YES,
    NO,
    CANCEL,
    RETRY,
    OKAY
  };

  typedef alist<duple<const char *, diag_result_t>> button_list;
  extern button_list &yes_no_cancel;
  extern button_list &yes_no_retry;
  extern button_list &okay_cancel;
  extern button_list &okay;

  class dialog : public window {
  public:
    //button names are copied
    //alist isn't needed past constructor
    dialog(widget &top_part, button_list buttons);

    //zero means not set yet
    diag_result_t result;

    void show_modal();

  private:
    vbox *main_box;
  };
}

#endif