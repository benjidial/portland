#ifndef PLAND_SYSCALL_H
#define PLAND_SYSCALL_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

#include <winact.h>

typedef uint32_t _file_handle_t;
typedef uint32_t _task_handle_t;
typedef uint32_t _drive_number_t;
typedef void    *_window_handle_t;

typedef struct __attribute__ ((packed)) {
  char name[100];
  uint32_t size;
  bool is_dir;
  uint8_t pad[23];
} _dir_info_entry_t;

typedef struct __attribute__ ((packed)) {
  uint8_t r;
  uint8_t g;
  uint8_t b;
  uint8_t pad;
} _pixel_t;

enum _scn {
  _SCN_OPEN_FILE,
  _SCN_CLOSE_FILE,
  _SCN_FILE_READ,
  _SCN_FILE_SIZE,
  _SCN_START_TASK,
  _SCN_IPC_SEND,
  _SCN_IPC_READ,
  _SCN_ALLOCATE_RAM,
  _SCN_MEMORY_INFO,
  _SCN_WAIT_FOR_TASK,
  _SCN_ENUMERATE_DIR,
  _SCN_SYSTEM_LOG,
  _SCN_COUNT_OF_DIR,
  _SCN_NEW_WINDOW,
  _SCN_DELETE_WINDOW,
  _SCN_RESIZE_WINDOW,
  _SCN_REASSIGN_PIXBUF,
  _SCN_PAINT_WINDOW,
  _SCN_GET_WIN_ACTION,
  _SCN_WAIT_FOR_ACTION,
  _SCN_WAIT_IPC_SENT,
  _SCN_WAIT_ANY_IPC_SENT,
  _SCN_FIND_UNREAD_IPC,
  _SCN_WAIT_IPC_READ,
  _SCN_IS_TASK_RUNNING,
  _SCN_GET_TIMESTAMP,
  _SCN_FILE_WRITE,
  _SCN_SET_FILE_SIZE,
  _SCN_WANTS_MOUSE_MOVES
};

static inline uint32_t _sc0(enum _scn eax) {
  uint32_t out;
  asm volatile (
    "int $0x30"
  : "=a" (out) : "a" (eax) : "ecx", "edx");
  return out;
}

static inline uint32_t _sc1(enum _scn eax, uint32_t ebx) {
  uint32_t out;
  asm volatile (
    "int $0x30"
  : "=a" (out) : "a" (eax), "b" (ebx) : "ecx", "edx");
  return out;
}

static inline uint32_t _sc2(enum _scn eax, uint32_t ebx, uint32_t ecx) {
  uint32_t out;
  uint32_t dummy;
  asm volatile (
    "int $0x30"
  : "=a" (out), "=c" (dummy) : "a" (eax), "b" (ebx), "c" (ecx) : "edx");
  return out;
}

static inline uint32_t _sc3(enum _scn eax, uint32_t ebx, uint32_t ecx, uint32_t edx) {
  uint32_t out;
  uint32_t dummy;
  asm volatile (
    "int $0x30"
  : "=a" (out), "=c" (dummy), "=d" (dummy) : "a" (eax), "b" (ebx), "c" (ecx), "d" (edx));
  return out;
}

static inline uint32_t _sc4(enum _scn eax, uint32_t ebx, uint32_t ecx, uint32_t edx, uint32_t esi) {
  uint32_t out;
  uint32_t dummy;
  asm volatile (
    "int $0x30"
  : "=a" (out), "=c" (dummy), "=d" (dummy) : "a" (eax), "b" (ebx), "c" (ecx), "d" (edx), "S" (esi));
  return out;
}

static inline uint32_t _sc5(enum _scn eax, uint32_t ebx, uint32_t ecx, uint32_t edx, uint32_t esi, uint32_t edi) {
  uint32_t out;
  uint32_t dummy;
  asm volatile (
    "int $0x30"
  : "=a" (out), "=c" (dummy), "=d" (dummy) : "a" (eax), "b" (ebx), "c" (ecx), "d" (edx), "S" (esi), "D" (edi));
  return out;
}

static inline void _yield_task() {
  asm volatile (
    "int $0x39"
  : : : "eax");
}

__attribute__ ((noreturn))
static inline void _exit_task() {
  asm volatile (
    "int $0x38"
  );
  __builtin_unreachable();
}

static inline _file_handle_t _open_file(const char *path) {
  return (_file_handle_t)_sc1(_SCN_OPEN_FILE, (uint32_t)path);
}

static inline void _close_file(_file_handle_t handle) {
  _sc1(_SCN_CLOSE_FILE, handle);
}

static inline uint32_t _file_read(_file_handle_t handle, uint32_t file_offset, uint32_t count, void *buffer) {
  return _sc4(_SCN_FILE_READ, handle, file_offset, count, (uint32_t)buffer);
}

static inline uint32_t _file_size(_file_handle_t handle) {
  return _sc1(_SCN_FILE_SIZE, handle);
}

static inline _task_handle_t _start_task(const char *path, const char *pass, _task_handle_t stdio_task) {
  return (_task_handle_t)_sc3(_SCN_START_TASK, (uint32_t)path, (uint32_t)pass, (uint32_t)stdio_task);
}

static inline uint32_t _ipc_send(_task_handle_t handle, uint32_t count, const void *buffer) {
  return _sc3(_SCN_IPC_SEND, handle, count, (uint32_t)buffer);
}

static inline uint32_t _ipc_read(_task_handle_t handle, uint32_t count, void *buffer) {
  return _sc3(_SCN_IPC_READ, handle, count, (uint32_t)buffer);
}

static inline void *_allocate_ram(uint32_t pages) {
  return (void *)_sc1(_SCN_ALLOCATE_RAM, pages);
}

static inline uint32_t _kernel_dynamic_area_size() {
  return _sc1(_SCN_MEMORY_INFO, 0x0);
}

static inline uint32_t _kernel_dynamic_area_left() {
  return _sc1(_SCN_MEMORY_INFO, 0x1);
}

static inline uint32_t _total_userspace_size() {
  return _sc1(_SCN_MEMORY_INFO, 0x2);
}

static inline uint32_t _total_userspace_left() {
  return _sc1(_SCN_MEMORY_INFO, 0x3);
}

static inline uint32_t _this_process_memory_left() {
  return _sc1(_SCN_MEMORY_INFO, 0x4);
}

static inline void _wait_for_task(_task_handle_t handle) {
  _sc1(_SCN_WAIT_FOR_TASK, handle);
}

static inline uint32_t _enumerate_dir(const char *path, _dir_info_entry_t *buffer, uint32_t max_count) {
  return _sc3(_SCN_ENUMERATE_DIR, (uint32_t)path, (uint32_t)buffer, max_count);
}

static inline uint32_t _count_of_dir(const char *path) {
  return _sc1(_SCN_COUNT_OF_DIR, (uint32_t)path);
}

static inline _window_handle_t _new_window(uint16_t width, uint16_t height, _pixel_t *pixel_buffer) {
  return (_window_handle_t)_sc3(_SCN_NEW_WINDOW, width, height, (uint32_t)pixel_buffer);
}

static inline void _delete_window(_window_handle_t window) {
  _sc1(_SCN_DELETE_WINDOW, (uint32_t)window);
}

static inline void _resize_window(_window_handle_t window, uint16_t width, uint16_t height, const _pixel_t *pixel_buffer) {
  _sc4(_SCN_RESIZE_WINDOW, (uint32_t)window, width, height, (uint32_t)pixel_buffer);
}

static inline void _reassign_pixbuf(_window_handle_t window, const _pixel_t *pixel_buffer) {
  _sc2(_SCN_REASSIGN_PIXBUF, (uint32_t)window, (uint32_t)pixel_buffer);
}

static inline void _paint_window(_window_handle_t window) {
  _sc1(_SCN_PAINT_WINDOW, (uint32_t)window);
}

static inline void _get_win_action(_window_handle_t window, struct window_action *action_pointer) {
  _sc2(_SCN_GET_WIN_ACTION, (uint32_t)window, (uint32_t)action_pointer);
}

static inline void _wait_for_action() {
  _sc0(_SCN_WAIT_FOR_ACTION);
}

static inline void _wait_ipc_sent(_task_handle_t sending_task) {
  _sc1(_SCN_WAIT_IPC_SENT, sending_task);
}

static inline void _system_log(const char *sz) {
  _sc1(_SCN_SYSTEM_LOG, (uint32_t)sz);
}

static inline void _wait_for_any_ipc_sent() {
  _sc0(_SCN_WAIT_ANY_IPC_SENT);
}

static inline _task_handle_t _find_unread_ipc() {
  return _sc0(_SCN_FIND_UNREAD_IPC);
}

static inline void _wait_ipc_read(_task_handle_t reading_task) {
  _sc1(_SCN_WAIT_IPC_READ, reading_task);
}

static inline bool _is_task_running(_task_handle_t handle) {
  return (bool)_sc1(_SCN_IS_TASK_RUNNING, handle);
}

static inline uint32_t _get_timestamp() {
  return _sc0(_SCN_GET_TIMESTAMP);
}

static inline uint32_t _file_write(_file_handle_t handle, uint32_t offset, uint32_t count, const void *from) {
  return _sc4(_SCN_FILE_WRITE, handle, offset, count, (uint32_t)from);
}

static inline void _set_file_size(_file_handle_t handle, uint32_t new_size) {
  _sc2(_SCN_SET_FILE_SIZE, handle, new_size);
}

static inline void _wants_mouse_moves(_window_handle_t window) {
  _sc1(_SCN_WANTS_MOUSE_MOVES, (uint32_t)window);
}

#ifdef __cplusplus
}
#endif

#endif