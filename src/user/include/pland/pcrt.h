#ifndef PLAND_PCRT_H
#define PLAND_PCRT_H

#ifdef __cplusplus
extern "C" {
#endif

#include <pland/syscall.h>

#define BEFORE_MAIN(f) \
  __attribute__ ((section (".__pcrt_before_main"))) \
  __attribute__ ((used)) \
  void (*const __pcrt_bm_##f)() = &f;

#define BEFORE_QUIT(f) \
  __attribute__ ((section (".__pcrt_before_quit"))) \
  __attribute__ ((used)) \
  void (*const __pcrt_bq_##f)() = &f;

void __pcrt_quit() __attribute__ ((noreturn));

extern _task_handle_t calling_task;
extern _task_handle_t stdio_task;
extern _task_handle_t this_task;

#ifdef __cplusplus
}
#endif

#endif
