bits 32

global __pcrt_entry
global __pcrt_quit
global calling_task
global stdio_task
global this_task

extern main
extern __pcrt_before_main_start
extern __pcrt_before_main_end
extern __pcrt_before_quit_start
extern __pcrt_before_quit_end

section .text
__pcrt_entry:
  mov dword [calling_task], edi
  mov dword [stdio_task], esi
  mov dword [this_task], ecx
  push edx

  mov ebx, __pcrt_before_main_start
.before_main_loop:
  cmp ebx, __pcrt_before_main_end
  je .call_main
  call dword [ebx]
  add ebx, 4
  jmp .before_main_loop

.call_main:
  call main

__pcrt_quit:
  mov ebx, __pcrt_before_quit_start
.before_quit_loop:
  cmp ebx, __pcrt_before_quit_end
  je .end_task
  call dword [ebx]
  add ebx, 4
  jmp .before_quit_loop

.end_task:
  int 0x38

section .bss
calling_task resd 1
stdio_task resd 1
this_task resd 1