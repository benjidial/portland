#include <keypack.h>
#include <stdbool.h>

static const char no_mod[] = {
   0,   0,   0,   0,   0,   0,   0,   0,   0, '\t','\n',  0,   0,   0,   0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
  ' ',  0,   0,   0,   0,   0,   0, '\'',  0,   0,   0,   0,  ',', '-', '.', '/',
  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',  0,  ';',  0,  '=',  0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  '[','\\', ']',  0,   0,
  '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
  'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',  0,   0,   0,   0,   0
};

static const char shifted[] = {
   0,   0,   0,   0,   0,   0,   0,   0,   0, '\t','\n',  0,   0,   0,   0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
  ' ',  0,   0,   0,   0,   0,   0,  '"',  0,   0,   0,   0,  '<', '_', '>', '?',
  ')', '!', '@', '#', '$', '%', '^', '&', '*', '(',  0,  ':',  0,  '+',  0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  '{', '|', '}',  0,   0,
  '~', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
  'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',  0,   0,   0,   0,   0
};

static const char caps[] = {
   0,   0,   0,   0,   0,   0,   0,   0,   0, '\t','\n',  0,   0,   0,   0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
  ' ',  0,   0,   0,   0,   0,   0, '\'',  0,   0,   0,   0,  ',', '-', '.', '/',
  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',  0,  ';',  0,  '=',  0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  '[','\\', ']',  0,   0,
  '`', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
  'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',  0,   0,   0,   0,   0
};

static const char sh_caps[] = {
   0,   0,   0,   0,   0,   0,   0,   0,   0, '\t','\n',  0,   0,   0,   0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
  ' ',  0,   0,   0,   0,   0,   0,  '"',  0,   0,   0,   0,  '<', '_', '>', '?',
  ')', '!', '@', '#', '$', '%', '^', '&', '*', '(',  0,  ':',  0,  '+',  0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  '{', '|', '}',  0,   0,
  '~', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
  'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',  0,   0,   0,   0,   0
};

static const char num[] = {
  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '*', '+', '\n', '-', '.', '/'
};

__attribute__ ((const))
char key_to_char(struct key_packet kp) {
  if (kp.key_id < 0x80) {
    const char ch = (kp.modifiers & SHIFTS
               ? kp.modifiers & CAPS
                 ? sh_caps : shifted
               : kp.modifiers & CAPS
                 ? caps : no_mod)[kp.key_id];
    return ch ? kp.modifiers & ALTS ? 0x80 & ch : ch : 0;
  }

  else if ((kp.modifiers & NUM) && ((kp.key_id & 0xf0) == 0xb0))
    return num[kp.key_id & 0x0f];

  else
    return 0;
}

__attribute__ ((const))
bool match_side_agnostic(struct key_packet a, struct key_packet b) {
  return (a.key_id == b.key_id) &&
    ((bool)(a.modifiers & SHIFTS) == (bool)(b.modifiers & SHIFTS)) &&
    ((bool)(a.modifiers & CTRLS)  == (bool)(b.modifiers & CTRLS))  &&
    ((bool)(a.modifiers & ALTS)   == (bool)(b.modifiers & ALTS))   &&
    ((bool)(a.modifiers & WINS)   == (bool)(b.modifiers & WINS));
}