#include <pland/syscall.h>
#include <knob/file.h>
#include <knob/heap.h>
#include <knob/block.h>

#include <stdbool.h>

#include <knob/format.h>

_task_handle_t run_command(const char *path, _task_handle_t stdio_task) {
  for (const char *ptr = path; *ptr; ++ptr)
    if (*ptr == ' ') {
      char *new_path = get_block(ptr - path + 1);
      blockcpy(new_path, path, ptr - path);
      new_path[ptr - path] = '\0';

      _task_handle_t handle = _start_task(new_path, ptr + 1, stdio_task);
      free_block(new_path);
      return handle;
    }

  return _start_task(path, "", stdio_task);
}

bool try_run_command_blocking(const char *path, _task_handle_t stdio_task) {
  _task_handle_t handle = run_command(path, stdio_task);
  if (!handle) {
    return false;
  }
  do {
    _wait_for_task(handle);
    _yield_task();
  } while (_is_task_running(handle));
  return true;
}