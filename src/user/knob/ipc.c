#include <pland/syscall.h>

//blocking, returns early if other process is dead.
//return value is number of bytes written.
uint32_t try_send_ipc(_task_handle_t to, const void *buffer, uint32_t size) {
  const uint32_t size_backup = size;
  while (size) {
  //syslogf("_ipc_send(0x%2h, 0x%h, %u)", to, buffer, size);
    uint32_t res = _ipc_send(to, size, buffer);
  //syslogf("=> %u", res);
    if (!res) {
      _wait_ipc_read(to);
      _yield_task();
    }
    else if (res == -1)
      return size_backup - size;
    else {
      size -= res;
      buffer += res;
    }
  }
  return size_backup;
}

//blocking, returns early if other process is dead.
//return value is number of bytes read.
uint32_t try_read_ipc(_task_handle_t from, void *buffer, uint32_t size) {
  const uint32_t size_backup = size;
  while (size) {
  //syslogf("_ipc_read(0x%2h, 0x%h, %u)", from, buffer, size);
    uint32_t res = _ipc_read(from, size, buffer);
  //syslogf("=> %u", res);
    if (!res) {
      _wait_ipc_sent(from);
      _yield_task();
    }
    else if (res == -1)
      return size_backup - size;
    else {
      size -= res;
      buffer += res;
    }
  }
  return size_backup;
}

void flush_ipc(_task_handle_t from) {
  uint8_t buf[4096];
  while (_ipc_read(from, 4096, buf))
    ;
}