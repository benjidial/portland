#include <knob/panic.h>
#include <knob/block.h>
#include <knob/heap.h>
#include <stdbool.h>
#include <stdint.h>

//unsophisticated, should copy by dwords where available
void blockcpy(void *to, const void *from, uint32_t size) {
  for (uint32_t i = 0; i < size; ++i)
    *(uint8_t *)(to++) = *(const uint8_t *)(from++);
}

//unsophisticated, should check by dwords wheere available
__attribute__ ((__pure__))
bool blockequ(const void *a, const void *b, uint32_t size) {
  for (uint32_t i = 0; i < size; ++i)
    if (*(uint8_t *)(a++) != *(uint8_t *)(b++))
      return false;
  return true;
}

//returns length without null-terminator
uint32_t strcpy(char *to, const char *from) {
  uint32_t i = 0;
  do
    to[i] = from[i];
  while (from[i++]);
  return i - 1;
}

char *strdup(const char *from) {
  const uint32_t len = strlen(from) + 1;
  char *buf = get_block(len);
  blockcpy(buf, from, len);
  return buf;
}

char *strndup(const char *from, uint32_t n) {
  uint32_t len = strlen(from) + 1;
  if (n < len) {
    char *buf = get_block(n + 1);
    blockcpy(buf, from, n);
    buf[n] = '\0';
    return buf;
  }
  char *buf = get_block(len);
  blockcpy(buf, from, len);
  return buf;
}

__attribute__ ((pure))
bool strequ(const char *a, const char *b) {
  while (true) {
    if ((*a == '\0') != (*b == '\0'))
      return false;
    if (*a == '\0')
      return true;
    if (*a != *b)
      return false;
    ++a;
    ++b;
  }
}

__attribute__ ((pure))
uint32_t strlen(const char *str) {
  uint32_t len = 0;
  while (*str) {
    ++len;
    ++str;
  }
  return len;
}

void str_trunc_fill(char *str, uint32_t len) {
  const uint8_t orig_len = strlen(str);
  if (orig_len > len) {
    str[len - 4] = ' ';
    str[len - 3] = '.';
    str[len - 2] = '.';
    str[len - 1] = '.';
    str[len] = '\0';
  }
  else if (orig_len != len) {
    for (uint8_t j = orig_len; j < len; ++j)
      str[j] = ' ';
    str[len] = '\0';
  }
}

__attribute__ ((pure))
uint32_t str_find_any(const char *str, const char *delims) {
  const char *i;
  for (i = str; *i; ++i)
    for (const char *j = delims; *j; ++j)
      if (*i == *j)
        return i - str;
  return i - str;
}