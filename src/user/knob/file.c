#include <knob/format.h>
#include <knob/block.h>
#include <knob/panic.h>
#include <knob/heap.h>

#include <pland/syscall.h>
#include <pland/pcrt.h>

struct ofl_node {
  struct ofl_node *next;
  struct ofl_node *prev;
  _file_handle_t handle;
};

static struct ofl_node *head_ofl_node = 0;

static void _close_all_files() {
  for (struct ofl_node *i = head_ofl_node; i; i = i->next)
    _close_file(i->handle);
}

BEFORE_QUIT(_close_all_files)

struct file {
  struct ofl_node *node;
  _file_handle_t handle;
  uint32_t position;
  uint32_t length;
};

struct file *open_file(const char *path) {
  _file_handle_t h = _open_file(path);
  if (!h)
    return 0;

  struct ofl_node *new_node = get_block(sizeof(struct ofl_node));
  new_node->next = head_ofl_node;
  new_node->prev = 0;
  new_node->handle = h;
  if (head_ofl_node)
    head_ofl_node->prev = new_node;
  head_ofl_node = new_node;

  struct file *f = get_block(sizeof(struct file));
  f->node = new_node;
  f->handle = h;
  f->position = 0;
  f->length = _file_size(h);

  return f;
}

void close_file(struct file *f) {
  _close_file(f->handle);
  struct ofl_node *n = f->node;
  if (n->next)
    n->next->prev = n->prev;
  if (n->prev)
    n->prev->next = n->next;
  if (n == head_ofl_node)
    head_ofl_node = n->next;
  free_block(n);
  free_block(f);
}

uint32_t read_from_file(struct file *f, uint32_t max, void *buf) {
  if (f->position + max > f->length)
    max = f->length - f->position;

  uint32_t read = _file_read(f->handle, f->position, max, buf);

  f->position += read;
  return read;
}

uint32_t write_to_file(struct file *f, uint32_t max, const void *buf) {
  if (f->position + max > f->length)
    _set_file_size(f->handle, f->length = f->position + max);

  uint32_t written = _file_write(f->handle, f->position, max, buf);

  f->position += written;
  return written;
}

//return value and max_length don't include null terminator
uint32_t read_line_from_file(struct file *f, char *sz, uint32_t max_length) {
  uint32_t i;
  for (i = 0; i < max_length; ++i) {
    char byte;
    if (!read_from_file(f, 1, &byte) || (byte == '\n'))
      break;
    sz[i] = byte;
  }
  sz[i] = '\0';
  return i;
}

uint32_t seek_file_to(struct file *f, uint32_t to) {
  if (to > f->length)
    to = f->length;
  return f->position = to;
}

int32_t seek_file_by(struct file *f, int32_t by) {
  uint32_t old = f->position;
  uint32_t to = old + by > f->length ? f->length : old + by;
  f->position = to;
  return to - old;
}

__attribute__ ((pure))
uint32_t get_file_pos(struct file *f) {
  return f->position;
}

__attribute__ ((pure))
uint32_t file_size(struct file *f) {
  return f->length;
}

void trunc_file(struct file *f) {
  _set_file_size(f->handle, f->length = f->position);
}

//return value must be manually freed, unless it is a null pointer
_dir_info_entry_t *get_directory_info(const char *path, uint32_t *count_out) {
  uint32_t count = _count_of_dir(path);
  if (!count) {
    *count_out = 0;
    return 0;
  }

  _dir_info_entry_t *buffer = get_block(count * sizeof(_dir_info_entry_t));
  *count_out = _enumerate_dir(path, buffer, count);
  return buffer;
}