#include <pland/syscall.h>
#include <pland/pcrt.h>

#include <stdint.h>

static uint32_t r;

static void seed_rand() {
  r = _get_timestamp();
}

BEFORE_MAIN(seed_rand)

uint32_t gen_rand() {
  r ^= r << 13;
  r ^= r >> 17;
  r ^= r << 5;
  return r;
}