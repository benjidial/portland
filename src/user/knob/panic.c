#include <knob/format.h>

#include <pland/syscall.h>
#include <pland/pcrt.h>

#include <stdint.h>

__attribute__ ((noreturn))
void panic(const char *filename, uint32_t line, const char *message) {
  _system_log(format("panic in %s on line %u: %s", filename, line, message));
  __pcrt_quit();
}