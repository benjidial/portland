#include <libterm/terminal.h>
#include <knob/file.h>

#define TEST_FILE "/user/test.txt"

void main() {
  struct file *f = open_file(TEST_FILE);
  if (!f) {
    term_add_sz("Failed to open " TEST_FILE ".\n");
    return;
  }

  char ch;
  if (!read_from_file(f, 1, &ch)) {
    term_add_sz(TEST_FILE " is empty.\n");
    close_file(f);
    return;
  }

  term_addf(TEST_FILE " contained '%c'.\n", ch);

  if (++ch >= 0x7f)
    ch = 0x21;

  seek_file_to(f, 0);

  if (!write_to_file(f, 1, &ch)) {
    term_add_sz("Failed to write to " TEST_FILE ".\n");
    close_file(f);
    return;
  }

  term_addf("Wrote '%c' to " TEST_FILE ".\n", ch);

  close_file(f);
}