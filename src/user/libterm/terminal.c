#include <libterm/command.h>

#include <knob/format.h>
#include <knob/block.h>
#include <knob/ipc.h>

_task_handle_t term_task;

void term_set_dimensions(uint32_t width, uint32_t height) {
  struct terminal_command cmd = {
    .kind = SET_DIMENSIONS,
    .as_coords = {
      .x = width,
      .y = height
    }
  };

  try_send_command(&cmd);
}

void term_get_dimensions(uint32_t *width, uint32_t *height) {
  struct terminal_command cmd = {
    .kind = GET_DIMENSIONS
  };

  if (try_send_command(&cmd)) {
    union terminal_response rs;
    if (try_get_response(&rs)) {
      *width = rs.as_coords.x;
      *height = rs.as_coords.y;
    }
  }
}

void term_paint() {
  struct terminal_command cmd = {
    .kind = PAINT
  };

  try_send_command(&cmd);
}

void term_clear() {
  struct terminal_command cmd = {
    .kind = CLEAR
  };

  try_send_command(&cmd);
}

void term_set_color(_pixel_t fg, _pixel_t bg) {
  struct terminal_command cmd = {
    .kind = SET_COLOR,
    .as_color = {
      .fg = fg,
      .bg = bg
    }
  };

  try_send_command(&cmd);
}

void term_set_cursor(uint32_t new_y, uint32_t new_x) {
  struct terminal_command cmd = {
    .kind = SET_CURSOR,
    .as_coords = {
      .y = new_y,
      .x = new_x
    }
  };

  try_send_command(&cmd);
}

void term_cursor_left() {
  struct terminal_command cmd = {
    .kind = CURSOR_LEFT
  };

  try_send_command(&cmd);
}

void term_cursor_right() {
  struct terminal_command cmd = {
    .kind = CURSOR_RIGHT
  };

  try_send_command(&cmd);
}

void term_cursor_up() {
  struct terminal_command cmd = {
    .kind = CURSOR_UP
  };

  try_send_command(&cmd);
}

void term_cursor_down() {
  struct terminal_command cmd = {
    .kind = CURSOR_DOWN
  };

  try_send_command(&cmd);
}

void term_add_char(char ch) {
  struct terminal_command cmd = {
    .kind = ADD_CHAR,
    .as_char = ch
  };

  try_send_command(&cmd);
}

void term_add_sn_no_ww(const char *s, uint32_t n) {
  struct terminal_command cmd = {
    .kind = ADD_SN_NO_WORDWRAP,
    .as_uint = n
  };

  if (try_send_command(&cmd))
    try_send_ipc(term_task, s, n);
}

void term_add_sz_no_ww(const char *sz) {
  term_add_sn_no_ww(sz, strlen(sz));
}

void term_add_sz(const char *sz) {
  const uint32_t len = strlen(sz);

  struct terminal_command cmd = {
    .kind = ADD_SN,
    .as_uint = len
  };

  if (try_send_command(&cmd))
    try_send_ipc(term_task, sz, len);
}

void term_addf_no_ww_v(const char *fmt, va_list args) {
  char *const msg = format_v(fmt, args);
  term_add_sz_no_ww(msg);
  free_block(msg);
}

void term_addf_no_ww(const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);
  term_addf_no_ww_v(fmt, args);
  va_end(args);
}

void term_addf_v(const char *fmt, va_list args) {
  char *const msg = format_v(fmt, args);
  term_add_sz(msg);
  free_block(msg);
}

void term_addf(const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);
  term_addf_v(fmt, args);
  va_end(args);
}

struct key_packet term_get_key_blocking() {
  struct terminal_command cmd = {
    .kind = GET_KEY
  };

  try_send_command(&cmd);

  union terminal_response rs;
  try_get_response(&rs);

  return rs.as_key;
}
