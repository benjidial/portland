#include <libterm/command.h>

#include <pland/pcrt.h>

void set_term_task_to_stdio() {
  term_task = stdio_task;
}

BEFORE_MAIN(set_term_task_to_stdio);