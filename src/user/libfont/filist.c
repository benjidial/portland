#include <libfont/fonts.h>
#include <knob/block.h>
#include <knob/heap.h>

struct dict_entry {
  char *name;
  struct font_info *fi;
  struct dict_entry *prev;
} *last_entry = 0;

__attribute__ ((pure))
struct font_info *find_entry(const char *name) {
  for (struct dict_entry *i = last_entry; i; i = i->prev)
    if (strequ(i->name, name))
      return i->fi;
  return 0;
}

struct font_info *new_entry(const char *name) {
  struct dict_entry *const nde = get_block(sizeof(struct dict_entry));
  nde->name = strdup(name);
  nde->fi = get_block(sizeof(struct font_info));
  nde->prev = last_entry;
  last_entry = nde;
  return nde->fi;
}

void del_last() {//only called when last_entry isn't 0
  free_block(last_entry->name);
  free_block(last_entry->fi);
  free_block(last_entry);
  last_entry = last_entry->prev;
}