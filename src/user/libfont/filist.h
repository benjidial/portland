#ifndef LIBFONT_FILIST_H
#define LIBFONT_FILIST_H

struct font_info *find_entry(const char *name) __attribute__ ((pure));
struct font_info *new_entry(const char *name);
void del_last();

#endif