#ifndef LIBFONT_BDF_H
#define LIBFONT_BDF_H

#include <libfont/fonts.h>
#include <knob/file.h>

#include <stdbool.h>

bool try_load_bdf(struct file *f, struct font_info *into);

#endif