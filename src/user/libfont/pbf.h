#ifndef LIBFONT_PBF_H
#define LIBFONT_PBF_H

#include <libfont/fonts.h>
#include <knob/file.h>

#include <stdbool.h>

bool try_load_pbf(struct file *f, struct font_info *into);

#endif