#include <libfont/fonts.h>

#include <knob/panic.h>
#include <knob/file.h>
#include <knob/heap.h>

#include <stdbool.h>

bool try_load_pbf(struct file *f, struct font_info *into) {
  uint8_t head[4];
  if (read_from_file(f, 4, head) != 4)
    return false;

  into->space_width  = head[0] + head[2];
  into->space_height = head[1] + head[3];
  into->char_width   = head[0];
  into->char_height  = head[1];

  const uint16_t bm_size  =  head[0] * head[1];
  const uint16_t bm_bytes = (bm_size - 1) / 8 + 1;

  uint32_t bm_offsets[256];
  if (read_from_file(f, 4 * 256, bm_offsets) != 4 * 256)
    return false;

  uint8_t bm_buf[256 * 256 / 8];

  for (uint16_t i = 0; i < 256; ++i)
    if (bm_offsets[i] == 0xffffffff)
      into->bitmaps[i] = 0;
    else {
      bool *bp = get_block(bm_size);
      if (!bp)
        PANIC("couldn't allocate memory in pbf loader (todo: fail gracefully)");
      seek_file_to(f, 4 + 4 * 256 + bm_offsets[i]);
      read_from_file(f, bm_bytes, bm_buf);
      for (uint16_t j = 0; j < bm_size; ++j)
        bp[j] = (bm_buf[j / 8] >> (j % 8)) & 1;
      into->bitmaps[i] = bp;
    }

  return true;
}