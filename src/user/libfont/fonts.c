#include <libfont/fonts.h>
#include <knob/format.h>
#include <knob/block.h>
#include <knob/heap.h>
#include <knob/file.h>

#include "filist.h"
#include "bdf.h"
#include "pbf.h"

#define FONT_PATH "/fonts/"
#define FONT_PATH_L 7

struct font_loader_t {
  const char *ext;
  bool (*func)(struct file *f, struct font_info *);
} font_loaders[] = {
  { .ext = ".pbf",
    .func = try_load_pbf
  },
  { .ext = ".bdf",
    .func = try_load_bdf
  },
  { .ext = ""
  }
};

struct font_info *get_font(const char *name) {
  struct font_info *font = find_entry(name);
  if (font)
    return font;
  font = new_entry(name);
  if (!font)
    return 0;//out of memory?

  const uint32_t name_len = strlen(name);
  for (struct font_loader_t *i = font_loaders; i->ext[0]; ++i) {
    char *buf = get_block(FONT_PATH_L + name_len + strlen(i->ext) + 1);
    blockcpy(buf, FONT_PATH, FONT_PATH_L);
    blockcpy(buf + FONT_PATH_L, name, name_len);
    strcpy(buf + FONT_PATH_L + name_len, i->ext);
    struct file *f = open_file(buf);
    free_block(buf);
    if (!f)
      continue;
  //syslogf("[libfont] Loading %s%s...", name, i->ext);
    if (i->func(f, font)) {
      close_file(f);
    //syslogf("[libfont] Loaded %s%s.", name, i->ext);
      return font;
    }
    close_file(f);
  //syslogf("[libfont] Failed to load %s%s.", name, i->ext);
  }
  del_last();
  return 0;
}

//pitch is in pixels
void put_char(const struct font_info *font, char ch, _pixel_t *pb_ptr, uint32_t pb_pitch, _pixel_t bg, _pixel_t fg) {
  const bool *const bitmap = font->bitmaps[(uint8_t)ch] ? font->bitmaps[(uint8_t)ch] : font->bitmaps[0];
  for (uint32_t y = 0; y < font->char_height; ++y)
    for (uint32_t x = 0; x < font->char_width; ++x)
      pb_ptr[y * pb_pitch + x] = bitmap[y * font->char_width + x] ? fg : bg;
}

//pitch is in pixels
void put_char_no_bg(const struct font_info *font, char ch, _pixel_t *pb_ptr, uint32_t pb_pitch, _pixel_t fg) {
  const bool *const bitmap = font->bitmaps[(uint8_t)ch] ? font->bitmaps[(uint8_t)ch] : font->bitmaps[0];
  for (uint32_t y = 0; y < font->char_height; ++y)
    for (uint32_t x = 0; x < font->char_width; ++x)
      if (bitmap[y * font->char_width + x])
        pb_ptr[y * pb_pitch + x] = fg;
}