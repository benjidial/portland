#include <raleigh/w/label.h>
#include <raleigh/runtime.h>
#include <raleigh/window.h>
#include <pland/syscall.h>
#include <knob/format.h>

using namespace raleigh;

label *l;

void refresh(window_tag_t) {
  char *fmt = format("Kernel memory free: %ukB\nUser memory free: %ukB\nF5 to refresh, Alt+F4 to quit", _kernel_dynamic_area_left() * 4, _total_userspace_left() * 4);
  l->change_value(fmt);
  free_block(fmt);
}

void main() {
  l = new label("");

  window w(*l);
  w.add_keybind((key_packet){.key_id = key_packet::KEY_F5, .modifiers = key_packet::NO_MODS}, &refresh);
  refresh(0);

  w.show();
  start_runtime();
}