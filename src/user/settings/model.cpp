#include <raleigh/util.h>

#include "string_kind.h"
#include "color_kind.h"

#define N_SETTING_KINDS 2
const setting_kind_info *setting_kinds[] = {
  &string_kind,
  &color_kind
};

struct file_head {
  uint32_t main_start;
  uint32_t count;
  uint32_t names_start;
  uint32_t data_start;
} __attribute__ ((__packed__));

struct setting_head {
  uint32_t name_offset;
  uint8_t name_length;
  uint8_t type;
} __attribute__ ((__packed__));

settings_model::settings_model(const char *from_file)
  : settings() {
  file *f = open_file(from_file);

  file_head header;
  read_from_file(f, 16, &header);
  settings.expand_to(header.count);
  settings.n_entries = header.count;

  for (uint32_t i = 0; i < header.count; ++i) {
    setting_head shead;
    seek_file_to(f, header.main_start + i * 16);
    read_from_file(f, sizeof(setting_head), &shead);

    char *name = new char[shead.name_length + 1];
    seek_file_to(f, header.names_start + shead.name_offset);
    read_from_file(f, shead.name_length, name);
    name[shead.name_length] = '\0';

    if (shead.type >= N_SETTING_KINDS)
      raleigh::show_error_popup_and_quitf("setting \"%s\" has unkown type 0x%2x.", name, shead.type);

    setting ns;
    ns.name = name;
    ns.kind = setting_kinds[shead.type];

    seek_file_to(f, header.main_start + i * 16 + 8);
    ns.backing = ns.kind->read_backing(f, header.data_start);

    settings.buf[i] = ns;
  }

  close_file(f);
}