#include "color_kind.h"

#include <raleigh/w/colorpicker.h>

void write_color_main(uint32_t &data_offset, file *f, const backing_t backing) {
  _pixel_t main[2];
  main[0] = *(const _pixel_t *)backing;
  write_to_file(f, 8, main);
}

void write_color_data(file *f, const backing_t backing) {}

backing_t read_color_backing(file *f, uint32_t data_start) {
  _pixel_t *p = new _pixel_t[1];
  read_from_file(f, 3, p);
  return (backing_t)p;
}

void load_into_picker(raleigh::colorpicker &e, _pixel_t *s) {
  e.set_picked_color(*s);
}

void save_from_picker(raleigh::colorpicker &e, _pixel_t *&s) {
  *s = e.get_picked_color();
}

void open_color_editor(setting &s) {
  do_common_editor<_pixel_t *, raleigh::colorpicker, &load_into_picker, &save_from_picker>(s);
}

const setting_kind_info color_kind = {
  &write_color_main,
  &write_color_data,
  &read_color_backing,
  &open_color_editor
};