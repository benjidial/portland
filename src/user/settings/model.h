#ifndef MODEL_H
#define MODEL_H

#include <raleigh/d/dialog.h>
#include <raleigh/w/button.h>
#include <raleigh/w/label.h>
#include <raleigh/w/vbox.h>
#include <raleigh/window.h>
#include <structs/map.h>
#include <knob/file.h>

typedef void *backing_t;

struct setting;

struct setting_kind_info {
  void (*write_main)(uint32_t &data_offset, file *f, const backing_t backing);
  void (*write_data)(file *f, const backing_t backing);
  backing_t (*read_backing)(file *f, uint32_t data_start);
  void (*open_editor)(setting &s);
};

struct setting {
  const char *name;
  const setting_kind_info *kind;
  void *backing;
};

class settings_model {
public:
  settings_model(const char *from_file);

  void write_to_file(const char *to_file);

  alist<setting> settings;
};

using namespace raleigh;

template<class backing_type, class editing_widget, void (*load_into_widget)(editing_widget &ew, backing_type s), void (*save_from_widget)(editing_widget &ew, backing_type &s)>
void do_common_editor(setting &s) {
  dllist<widget &> widgets;

  label l(s.name);
  widgets.add_back(l);

  editing_widget ew;
  load_into_widget(ew, (backing_type)s.backing);
  widgets.add_back(ew);

  vbox box(widgets);

  dialog d(box, okay_cancel);
  d.show_modal();

  if (d.result == OKAY)
    save_from_widget(ew, (backing_type &)s.backing);
}

#endif