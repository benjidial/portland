#include "string_kind.h"

#include <raleigh/w/entry.h>

void write_string_main(uint32_t &data_offset, file *f, const backing_t backing) {
  const uint32_t len = strlen((const char *)backing);

  uint32_t main[2];
  main[0] = data_offset;
  main[1] = len;

  data_offset += len;

  write_to_file(f, 8, main);
}

void write_string_data(file *f, const backing_t backing) {
  const char *b = (const char *)backing;
  write_to_file(f, strlen(b), b);
}

backing_t read_string_backing(file *f, uint32_t data_start) {
  uint32_t main[2];
  read_from_file(f, 8, main);

  seek_file_to(f, data_start + main[0]);

  char *buf = new char[main[1] + 1];
  read_from_file(f, main[1], buf);
  buf[main[1]] = '\0';

  return (backing_t)buf;
}

void load_into_entry(raleigh::entry &e, const char *s) {
  e.set_contents(s);
}

void save_from_entry(raleigh::entry &e, const char *&s) {
  s = e.get_contents();
}

void open_string_editor(setting &s) {
  do_common_editor<const char *, raleigh::entry, &load_into_entry, &save_from_entry>(s);
}

const setting_kind_info string_kind = {
  &write_string_main,
  &write_string_data,
  &read_string_backing,
  &open_string_editor
};