#include "model.h"

#include <raleigh/w/button.h>
#include <raleigh/w/label.h>
#include <raleigh/w/vbox.h>

#define SETTINGS_FILE "/sys/settings.pls"

using namespace raleigh;

void on_button(button_tag_t tag) {
  setting *s = (setting *)tag;
  s->kind->open_editor(*s);
}

void main() {
  settings_model sm(SETTINGS_FILE);

  label instructions("Click a button below to edit that setting.");

  dllist<widget &> box_widgets;
  box_widgets.add_back(instructions);

  for (uint32_t i = 0; i < sm.settings.n_entries; ++i) {
    label *l = new label(sm.settings.buf[i].name);
    button *b = new button(*l, &on_button, (button_tag_t)&sm.settings.buf[i]);
    box_widgets.add_back(*b);
  }

  vbox box(box_widgets);
  window w(box);
  w.show();

  start_runtime();
}