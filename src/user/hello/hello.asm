bits 32

global _entry

section .text
_entry:
  mov eax, 0x05
  mov ebx, esi
  mov ecx, data.len
  mov edx, data
  int 0x30

  int 0x38

section .rodata
data:
  dd 0xb
  dd .str_len
  dd 0

.str:
  db "Hello, world!", 0x0a
.str_len equ $ - .str

  dd 0x02
  dd 0
  dd 0
.len equ $ - data