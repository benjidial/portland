#include <libterm/terminal.h>
#include <knob/time.h>

static const char *const month_names[] = {
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
};

void main() {
  const struct time t = get_time();
  term_addf("TS: 0x%8h\n", t.timestamp);
  term_addf("Date: %s %u, %u\n", month_names[t.month - 1], t.day, t.year);
  term_addf("Time: %u:%2u:%2u\n", t.hour, t.minute, t.second);
}