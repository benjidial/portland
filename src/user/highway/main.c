#include <libterm/terminal.h>
#include <libterm/readline.h>

#include <knob/task.h>

#include "cmds.h"
#include "line.h"
#include "vars.h"

void main(const char *arg) {
  source(*arg ? arg : "/user/default.rc");
  ensure_color();

  term_add_sz("Portland Highway\nType \"help\" for help.\n");
  term_paint();

  struct history *h = new_history(200);

  char cmd_buf[128];
  while (1) {
    read_line(cmd_buf, 127, "> ", h);
    run_line(cmd_buf);
  }
}