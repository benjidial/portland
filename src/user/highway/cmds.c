#include <libterm/terminal.h>

#include <knob/file.h>

#include "line.h"
#include "vars.h"

void source(const char *path) {
  struct file *f = open_file(path);
  if (!f) {
    term_addf("Could not open %s.\n", path);
    return;
  }
  char buf[128];
  while (read_line_from_file(f, buf, 127))
    run_line(buf);
  close_file(f);
}

void set(const char *arg) {
  const char *space = arg;
  while (*space != ' ')
    if (*space)
      ++space;
    else {
      struct no_null_sn vname = {
        .data = arg,
        .length = space - arg
      };
      del_var(vname);
      return;
    }

  struct no_null_sn vname = {
    .data = arg,
    .length = space - arg
  };

  const char *vstart = space + 1,
             *vend = vstart;
  while (*vend)
    ++vend;
  
  struct no_null_sn vval = {
    .data = vstart,
    .length = vend - vstart
  };

  set_var(vname, vval);
}