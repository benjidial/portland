#include <libterm/terminal.h>

#include <knob/format.h>
#include <knob/block.h>
#include <knob/heap.h>
#include <knob/rand.h>

#include <pland/pcrt.h>

struct no_null_sn {
  char *data;
  uint32_t length;
};

struct var_dict_node {
  struct var_dict_node *next;
  struct var_dict_node *prev;
  struct no_null_sn name;
  struct no_null_sn value;
};

static struct var_dict_node *var_dict_start = 0;

__attribute__ ((pure))
static struct var_dict_node *get_node(struct no_null_sn name) {
  for (struct var_dict_node *i = var_dict_start; i; i = i->next)
    if ((i->name.length == name.length) &&
        blockequ(i->name.data, name.data, name.length))
      return i;
  return 0;
}

static struct var_dict_node *new_node() {
  struct var_dict_node *node = get_block(sizeof(struct var_dict_node));
  node->prev = 0;
  node->next = var_dict_start;
  if (var_dict_start)
    var_dict_start->prev = node;
  var_dict_start = node;
  return node;
}

void set_var(struct no_null_sn name, struct no_null_sn value) {
  struct var_dict_node *node = get_node(name);
  if (node)
    free_block(node->value.data);
  else {
    node = new_node();
    node->name.length = name.length;
    node->name.data = get_block(name.length);
    blockcpy(node->name.data, name.data, name.length);
  }
  node->value.length = value.length;
  node->value.data = get_block(value.length);
  blockcpy(node->value.data, value.data, value.length);
}

__attribute__ ((pure))
const struct no_null_sn *get_var(struct no_null_sn name) {
  struct var_dict_node *node = get_node(name);
  return node ? &node->value : 0;
}

void del_var(struct no_null_sn name) {
  struct var_dict_node *node = get_node(name);
  if (!node)
    return;
  free_block(node->name.data);
  free_block(node->value.data);
  if (node->prev)
    node->prev->next = node->next;
  if (node->next)
    node->next->prev = node->prev;
  if (node == var_dict_start)
    var_dict_start = node->next;
  free_block(node);
}

void dump_vars() {
  for (struct var_dict_node *node = var_dict_start; node; node = node->next) {
    term_addf_no_ww("$%ns$\t= %ns\n", node->name.length, node->name.data, node->value.length, node->value.data);
    term_paint();
  }
}

static const char hex_digits[] = "0123456789abcdef";
static char color[12] = "000000......";

static const struct no_null_sn color_name = {
  .data = "_color",
  .length = 6
};

static const struct no_null_sn color_value = {
  .data = color,
  .length = 12
};

void new_color() {
  const uint16_t hue = gen_rand() % (0x33 * 6);
  _pixel_t bg = {.r = 0xff, .g = 0xcc, .b = 0xcc};
  if (hue <= 0x33) {
    bg.b += hue;
    goto got_bg;
  }
  bg.b += 0x33;
  if (hue <= 0x33 * 2) {
    bg.r -= hue - 0x33;
    goto got_bg;
  }
  bg.r -= 0x33;
  if (hue <= 0x33 * 3) {
    bg.g += hue - 0x33 * 2;
    goto got_bg;
  }
  bg.g += 0x33;
  if (hue <= 0x33 * 4) {
    bg.b -= hue - 0x33 * 3;
    goto got_bg;
  }
  bg.b -= 0x33;
  if (hue <= 0x33 * 5) {
    bg.r += hue - 0x33 * 4;
    goto got_bg;
  }
  bg.r += 0x33;
  bg.g -= hue - 0x33 * 5;

got_bg:
  color[ 6] = hex_digits[bg.r / 16];
  color[ 7] = hex_digits[bg.r % 16];
  color[ 8] = hex_digits[bg.g / 16];
  color[ 9] = hex_digits[bg.g % 16];
  color[10] = hex_digits[bg.b / 16];
  color[11] = hex_digits[bg.b % 16];

  set_var(color_name, color_value);
}