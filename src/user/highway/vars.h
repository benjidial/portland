#ifndef VARS_H
#define VARS_H

#include <stdint.h>

struct no_null_sn {
  const char *data;
  uint32_t length;
};

void set_var(struct no_null_sn name, struct no_null_sn value);
const struct no_null_sn *get_var(struct no_null_sn name) __attribute__ ((pure));
void del_var(struct no_null_sn name);

void dump_vars();
void new_color();

#endif