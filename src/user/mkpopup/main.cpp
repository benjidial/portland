#include <raleigh/w/padding.h>
#include <raleigh/w/label.h>
#include <raleigh/window.h>

using namespace raleigh;

void main(const char *text) {
  char *unescaped = new char[strlen(text) + 1];
  char *ui = unescaped;
  while (1) {
    const uint32_t len = str_find_any(text, "\\");
    if (len) {
      blockcpy(ui, text, len);
      text += len;
      ui += len;
    }
    if (!*text)
      break;
    if (text[1] == 'n') {
      *(ui++) = '\n';
      text += 2;
    }
    else {
      *(ui++) = '\\';
      ++text;
    }
  }
  *ui = '\0';

  label l(unescaped);
  delete[] unescaped;
  padding p(l, 4);
  window w(p);

  w.show();
  start_runtime();
}