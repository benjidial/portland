#include <raleigh/runtime.h>
#include <raleigh/window.h>
#include <pland/pcrt.h>

namespace raleigh {
  dllist<window &> open_windows;
  dllist<window &> to_be_deleted;

  __attribute__ ((noreturn))
  void start_runtime() {
    while (1) {
      for (dllist<window &>::node *w = open_windows.first; w; w = w->next)
        w->d.consume_actions();
    l:
      for (dllist<window &>::node *w = to_be_deleted.first; w; w = w->next) {
        _delete_window(w->d.handle);
        w->d.handle = 0;
        open_windows.try_remove_by_ref(w->d);
        w = to_be_deleted.remove_in_place(w);
        if (!w)
          goto l;
      }
      if (!open_windows.first)
        __pcrt_quit();
      _wait_for_action();
      _yield_task();
    }
  }
}