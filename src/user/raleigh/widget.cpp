#include <raleigh/widget.h>

#include <knob/format.h>

namespace raleigh {
  widget::widget()
    : parent(0), w(0), next_paint_full(true) {}

  void widget::notify_window_change() {}
  void widget::handle_click(coord window_coords, enum mouse_packet::mouse_button click_type, bool up) {}
  void widget::handle_key(struct key_packet kp) {}
  void widget::on_focus() {}
  void widget::on_unfocus() {}
  void widget::on_mouse_move(coord window_coords) {}
  void widget::notify_child_size_change(widget &child, coord old_size) {}

  void widget::set_size(coord new_size) {
    coord old_size = size;
    size = new_size;
    if (w)
      w->notify_widget_size_change(*this, old_size);
  }
}