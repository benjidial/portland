#include <raleigh/w/hbox.h>

namespace raleigh {
  hbox::hbox(dllist<widget &> widgets)
    : multicontainer(widgets) {
    size = determine_size();
  }

  coord hbox::determine_size() {
    uint32_t w = 0, h = 0;
    for (dllist<widget &>::node *n = widgets.first; n; n = n->next) {
      n->d.parent = this;
      w += n->d.size.x;
      if (n->d.size.y > h)
        h = n->d.size.y;
    }
    return coord(w, h);
  }

  void hbox::set_child_offsets() {
    uint32_t x = window_offset.x;
    for (dllist<widget &>::node *n = widgets.first; n; n = n->next) {
      n->d.w = w;
      n->d.window_offset = coord(x, window_offset.y + size.y / 2 - n->d.size.y / 2);
      n->d.notify_window_change();
      x += n->d.size.x;
    }
  }
}