#include <raleigh/w/padding.h>

namespace raleigh {
  padding::padding(widget &inner, uint32_t pad_by)
    : inner(inner), pad_by(pad_by) {
    size = coord(inner.size.x + pad_by * 2, inner.size.y + pad_by * 2);
    closest_opaque = 0;
    inner.parent = this;
  }

  void padding::notify_window_change() {
    inner.w = w;
    inner.window_offset = coord(window_offset.x + pad_by, window_offset.y + pad_by);
    inner.notify_window_change();
  }

  void padding::paint(_pixel_t *pixbuf, uint32_t pitch) {
    if (next_paint_full) {
      next_paint_full = false;
      inner.next_paint_full = true;
    }
    inner.paint(pixbuf, pitch);
  }

  void padding::handle_click(coord window_coords, enum mouse_packet::mouse_button click_type, bool up) {
    if ((window_coords.x >= inner.window_offset.x) &&
        (window_coords.y >= inner.window_offset.y) &&
        (window_coords.x < inner.window_offset.x + inner.size.x) &&
        (window_coords.y < inner.window_offset.y + inner.size.y))
      inner.handle_click(window_coords, click_type, up);
  }

  void padding::notify_has_opaque_parent(widget *parent) {
    closest_opaque = parent;
    inner.notify_has_opaque_parent(parent);
  }

  void padding::notify_child_size_change(widget &child, coord old_size) {
    set_size(coord(inner.size.x + pad_by * 2, inner.size.y + pad_by * 2));
  }
}