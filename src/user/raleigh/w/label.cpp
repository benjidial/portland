#include <raleigh/w/label.h>
#include <libfont/fonts.h>
#include <knob/block.h>

namespace raleigh {
  coord label::determine_size() {
    uint32_t longest = 0;

    for (uint32_t i = 0; i < tf.get_n_lines(); ++i)
      if (strlen(tf.get_nth_line(i)) > longest)
        longest = strlen(tf.get_nth_line(i));

    const int32_t width  = fi->space_width  * (longest -          1) + fi->char_width;
    const int32_t height = fi->space_height * (tf.get_n_lines() - 1) + fi->char_height;
    return coord(width < 0 ? 0 : width, height < 0 ? 0 : height);
  }

  label::label(const char *value, const char *font, uint32_t cols,
               bool bg_transparent, _pixel_t fg, _pixel_t bg)
    : fi(get_font(font)), cols(cols), bg_transparent(bg_transparent),
      fg(fg), bg(bg), tf(strdup(value), cols) {

    size = determine_size();
    closest_opaque = bg_transparent ? 0 : this;
  }

  label::~label() {
    free_block(tf.s);
  }

  void label::change_value(const char *new_value) {
    free_block(tf.s);
    tf.s = strdup(new_value);
    tf.flow_text();
    set_size(determine_size());
    w->notify_needs_paint(*this);
  }

  void label::paint(_pixel_t *pixbuf, uint32_t pitch) {
    if (!bg_transparent)
      for (uint32_t y = window_offset.y; y < window_offset.y + size.y; ++y)
        for (uint32_t x = window_offset.x; x < window_offset.x + size.x; ++x)
          pixbuf[y * pitch + x] = bg;
    tf.draw_text(pixbuf + window_offset.y * pitch + window_offset.x, pitch, fi, fg);
  }

  void label::notify_has_opaque_parent(widget *parent) {
    if (bg_transparent)
      closest_opaque = parent;
  }
}