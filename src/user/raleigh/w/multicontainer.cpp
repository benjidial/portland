#include <raleigh/w/vbox.h>

namespace raleigh {
  multicontainer::multicontainer(dllist<widget &> widgets)
    : widgets(widgets) {
    closest_opaque = 0;
  }

  void multicontainer::notify_window_change() {
    set_child_offsets();
  }

  void multicontainer::paint(_pixel_t *pixbuf, uint32_t pitch) {
    for (dllist<widget &>::node *n = widgets.first; n; n = n->next) {
      if (next_paint_full)
        n->d.next_paint_full = true;
      n->d.paint(pixbuf, pitch);
    }
    if (next_paint_full)
      next_paint_full = false;
  }

  void multicontainer::handle_click(coord window_coords, enum mouse_packet::mouse_button click_type, bool up) {
    for (dllist<widget &>::node *n = widgets.first; n; n = n->next)
      if ((window_coords.x >= n->d.window_offset.x) &&
          (window_coords.y >= n->d.window_offset.y) &&
          (window_coords.x < n->d.window_offset.x + n->d.size.x) &&
          (window_coords.y < n->d.window_offset.y + n->d.size.y))
        n->d.handle_click(window_coords, click_type, up);
  }

  void multicontainer::notify_has_opaque_parent(widget *parent) {
    closest_opaque = parent;
    for (dllist<widget &>::node *n = widgets.first; n; n = n->next)
      n->d.notify_has_opaque_parent(parent);
  }

  void multicontainer::notify_child_size_change(widget &from, coord old_size) {
    set_size(determine_size());
    set_child_offsets();
  }

  void multicontainer::add_end(widget &n) {
    widgets.add_back(n);
    set_size(determine_size());
    set_child_offsets();
    if (w)
      w->notify_needs_paint(*this);
  }

  void multicontainer::add_start(widget &n) {
    widgets.add_front(n);
    set_size(determine_size());
    set_child_offsets();
    if (w)
      w->notify_needs_paint(*this);
  }
}