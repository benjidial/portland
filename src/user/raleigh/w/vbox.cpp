#include <raleigh/w/vbox.h>

namespace raleigh {
  vbox::vbox(dllist<widget &> widgets)
    : multicontainer(widgets) {
    size = determine_size();
  }

  coord vbox::determine_size() {
    uint32_t w = 0, h = 0;
    for (dllist<widget &>::node *n = widgets.first; n; n = n->next) {
      n->d.parent = this;
      h += n->d.size.y;
      if (n->d.size.x > w)
        w = n->d.size.x;
    }
    return coord(w, h);
  }

  void vbox::set_child_offsets() {
    uint32_t h = window_offset.y;
    for (dllist<widget &>::node *n = widgets.first; n; n = n->next) {
      n->d.w = w;
      n->d.window_offset = coord(window_offset.x + size.x / 2 - n->d.size.x / 2, h);
      n->d.notify_window_change();
      h += n->d.size.y;
    }
  }
}