#include <raleigh/w/button.h>

namespace raleigh {
  button::button(widget &inner, void (*on_click)(button_tag_t), button_tag_t tag,
    _pixel_t border_color, _pixel_t bg_color, _pixel_t pressed_color)
    : inner(inner), on_click(on_click), tag(tag), border_color(border_color),
      bg_color(bg_color), pressed_color(pressed_color), is_pressed(false) {
    size = coord(inner.size.x + 2, inner.size.y + 2);
    closest_opaque = this;
    inner.notify_has_opaque_parent(this);
    inner.parent = this;
  }

  void button::notify_window_change() {
    inner.window_offset = coord(window_offset.x + 1, window_offset.y + 1);
    inner.w = w;
    inner.notify_window_change();
  }

  void button::paint(_pixel_t *pixbuf, uint32_t pitch) {
    if (next_paint_full) {
      next_paint_full = false;
      for (uint32_t x = window_offset.x; x < window_offset.x + size.x; ++x) {
        pixbuf[window_offset.y * pitch + x] = border_color;
        pixbuf[(window_offset.y + size.y - 1) * pitch + x] = border_color;
      }
      for (uint32_t y = window_offset.y + 1; y < window_offset.y + size.y - 1; ++y) {
        pixbuf[y * pitch + window_offset.x] = border_color;
        pixbuf[y * pitch + window_offset.x + size.x - 1] = border_color;
      }
    }
    for (uint32_t y = window_offset.y + 1; y < window_offset.y + size.y - 1; ++y)
      for (uint32_t x = window_offset.x + 1; x < window_offset.x + size.x - 1; ++x)
        pixbuf[y * pitch + x] = is_pressed ? pressed_color : bg_color;
    inner.next_paint_full = true;
    inner.paint(pixbuf, pitch);
  }

  void button::handle_click(coord window_coords, enum mouse_packet::mouse_button click_type, bool up) {
    if (click_type != mouse_packet::LEFT)
      return;
    if (up) {
      is_pressed = false;
      inner.window_offset = coord(window_offset.x + 1, window_offset.y + 1);
      inner.notify_window_change();
      w->notify_needs_paint(*this);
      on_click(tag);
    }
    else {
      is_pressed = true;
      inner.window_offset = coord(window_offset.x + 2, window_offset.y + 2);
      inner.notify_window_change();
      w->notify_needs_paint(*this);
    }
  }

  void button::notify_child_size_change(widget &child, coord old_size) {
    set_size(coord(inner.size.x + 2, inner.size.y + 2));
  }

  void button::notify_has_opaque_parent(widget *parent) {}
}