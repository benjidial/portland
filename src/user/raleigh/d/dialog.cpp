#include <raleigh/d/dialog.h>

#include <raleigh/w/padding.h>
#include <raleigh/w/button.h>
#include <raleigh/w/label.h>
#include <raleigh/w/hbox.h>

using namespace raleigh;

void dialog::show_modal() {
  show();
  do {
    _wait_for_action();
    _yield_task();
    consume_actions();
  } while (!result);
  to_be_deleted.add_back(*this);
}

static bool on_diag_close(window_tag_t) {
  return false;
}

struct diag_and_result {
  dialog *d;
  diag_result_t r;
  diag_and_result(dialog *d, diag_result_t r)
    : d(d), r(r) {}
};

static void set_diag_result(button_tag_t tag) {
  const diag_and_result *const cast = (const diag_and_result *)tag;
  cast->d->result = cast->r;
  //TODO: go through all of raleigh and add destructors
  //in this case, delete all of these tags when the window is closed
}

dialog::dialog(widget &top_part, alist<duple<const char *, diag_result_t>> buttons)
  : window(*new vbox(), RGB(bf, bf, bf), &on_diag_close), result(0), main_box((vbox *)&root) {
  dllist<widget &> *button_row = new dllist<widget &>();
  for (duple<const char *, diag_result_t> *i = buttons.buf; i < buttons.buf + buttons.n_entries; ++i) {
    label *l = new label(i->a);
    padding *p = new padding(*l, 4);
    button *b = new button(*p, &set_diag_result, new diag_and_result(this, i->b));
    padding *pb = new padding(*b, 2);
    button_row->add_back(*pb);
  }
  hbox *button_box = new hbox(*button_row);

  padding *ptop = new padding(top_part, 2);
  padding *pbot = new padding(*button_box, 2);

  main_box->add_end(*ptop);
  main_box->add_end(*pbot);
}

button_list &mk_yes_no_cancel() {
  button_list *list = new button_list(3, 1);
  list->add_back(duple<const char *, diag_result_t>("Yes", YES));
  list->add_back(duple<const char *, diag_result_t>("No", NO));
  list->add_back(duple<const char *, diag_result_t>("Cancel", CANCEL));
  return *list;
}

button_list &mk_yes_no_retry() {
  button_list *list = new button_list(3, 1);
  list->add_back(duple<const char *, diag_result_t>("Yes", YES));
  list->add_back(duple<const char *, diag_result_t>("No", NO));
  list->add_back(duple<const char *, diag_result_t>("Retry", RETRY));
  return *list;
}

button_list &mk_okay_cancel() {
  button_list *list = new button_list(2, 1);
  list->add_back(duple<const char *, diag_result_t>("Okay", OKAY));
  list->add_back(duple<const char *, diag_result_t>("Cancel", CANCEL));
  return *list;
}

button_list &mk_okay() {
  button_list *list = new button_list(1, 1);
  list->add_back(duple<const char *, diag_result_t>("Okay", OKAY));
  return *list;
}

button_list &raleigh::yes_no_cancel(mk_yes_no_cancel());
button_list &raleigh::yes_no_retry(mk_yes_no_retry());
button_list &raleigh::okay_cancel(mk_okay_cancel());
button_list &raleigh::okay(mk_okay());