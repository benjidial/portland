#include <raleigh/d/saving_window.h>
#include <raleigh/d/dialog.h>

#include <raleigh/w/padding.h>
#include <raleigh/w/button.h>
#include <raleigh/w/label.h>
#include <raleigh/w/hbox.h>
#include <raleigh/w/vbox.h>

#include <structs/dllist.h>

namespace raleigh {
  bool on_saving_window_close(window_tag_t tag) {
    saving_window *const sw = (saving_window *)tag;
    if (sw->is_saved)
      return true;

    label text("There is unsaved content in this window.");
    label text2("Would you like to save before closing it?");

    dllist<widget &> rows;
    rows.add_back(text);
    rows.add_back(text2);
    vbox vb(rows);
    padding vbp(vb, 2);

    dialog diag(vbp, yes_no_cancel);
    diag.show_modal();

    switch (diag.result) {
    case YES:
    save:
      if (!sw->save_func(sw->save_tag)) {
        label text3("Failing saved. Still quit?");
        dialog diag2(text3, yes_no_retry);
        diag2.show_modal();
        switch (diag2.result) {
        case YES:
          return true;
        case RETRY:
          goto save;
        default:
          return false;
        }
      }
    case NO:
      return true;
    default:
      return false;
    }
  }

  saving_window::saving_window(bool (*save_func)(save_tag_t), save_tag_t save_tag, widget &root, _pixel_t bg_color)
    : window(root, bg_color, &on_saving_window_close, (window_tag_t)this), is_saved(true), save_func(save_func), save_tag(save_tag) {}
}