#include <raleigh/d/dialog.h>
#include <raleigh/w/label.h>
#include <raleigh/util.h>
#include <knob/format.h>
#include <pland/pcrt.h>

namespace raleigh {
  coord::coord(uint32_t x, uint32_t y)
    : x(x), y(y) {}
  coord::coord()
    : x(0), y(0) {}

  __attribute__ ((noreturn))
  void show_error_and_quitf(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);

    syslogf_v(fmt, args);

    __pcrt_quit();
  }

  __attribute__ ((noreturn))
  void show_error_popup_and_quitf(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);

    char *ch = format_v(fmt, args);
    label l(ch);

    dialog d(l, okay);
    d.show_modal();

    __pcrt_quit();
  }
}