#include <libterm/terminal.h>
#include <knob/block.h>
#include <knob/file.h>

#define MAX_NAME_LEN 15
#define COLUMN_SEP " - "

void main(const char *path) {
  uint32_t count;
  _dir_info_entry_t *entries = get_directory_info(path, &count);
  if (!entries) {
    term_addf("Could not list %s\n", path);
    return;
  }
  term_addf("Directory listing for %s:\n\n", *path ? path : "(root)");
  if (!count) {
    term_add_sz("(empty)\n");
    return;
  }
  for (_dir_info_entry_t *i = entries; i < entries + count; ++i) {
    str_trunc_fill(i->name, MAX_NAME_LEN);
    if (i->is_dir)
      term_addf_no_ww("%s" COLUMN_SEP "directory\n", i->name);
    else
      term_addf_no_ww("%s" COLUMN_SEP "%u bytes\n", i->name, i->size);
  }
}