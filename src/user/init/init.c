#include <knob/block.h>
#include <knob/file.h>
#include <knob/task.h>

#define STARTUP_FILE_PATH "/sys/startup.rc"
#define CMD_BUF_LEN 1024

char cmdbuf[CMD_BUF_LEN];

void main() {
  struct file *f = open_file(STARTUP_FILE_PATH);
  if (!f) {
    _system_log("Could not open " STARTUP_FILE_PATH ".\n");
    return;
  }

  while (read_line_from_file(f, cmdbuf, CMD_BUF_LEN - 1)) {
    if (cmdbuf[0] == '#')
      continue;
    bool started = (bool)run_command(cmdbuf, 0);
    str_trunc_fill(cmdbuf, 30);
    syslogf(started ? "Started %s" : "Could not start %s", cmdbuf);
  }

  close_file(f);

  _system_log("Done starting startup tasks.");
}
