## Application Name

(Put the name of the crashing application here. Do not report kernel panics with this template.)

## Steps to Reproduce

(Please tell us how to reliably make the crash happen.)

## Relevant Log Entries

(Please put anything interesting from the log file here, including any exception information. The log is currently not stored in a file, but output on COM1.)

## Version or Object Dump

(If using a released version, verify that the same steps still crash the application in the latest released version, and then put that version number here. If building from source, please verify that the same steps still crash the application when applying the same if any changes to the source code of the latest commit on master, and then put "master" here. Additionally, if you have made any source code modifications, after ensuring that the modifications aren't responsible for the crash, please link to an upload of the crashing application's `.elf` file from the `obj` directory.)

## Additional Comments

(If you've done any research into what might be causing the problem, or noticed a similarity to a past problem, please let us know!)

## Final Steps

(Please set the appropriate "where" label, then delete this heading. Finally, proofread your issue under the "preview" tab.)

/label ~"kind::application-crash"
