## Description

(Please describe the addition that you want. The more detailed, the better.)

## Rationale

(Why should this be added? Are there other potential additions that need this first?)

## Final Steps

(Please set the appropriate "where" label, then delete this heading. Finally, proofread your issue under the "preview" tab.)

/label ~"kind::addition"
