#args: .hex file, .pbf file, hex pitch, char width, char height, hz padding, vt padding
#converts the .hex output of gbdfed into a pbf file
#6x9x0x0

bitmaps = {}

File.readlines(ARGV[0]).map do |l|
  bitmaps[l[0..3].to_i 16] = (l[5..-2].to_i(16) + 2**(ARGV[2].to_i() * ARGV[4].to_i())).to_s(2)[1..-1]
end

data_area_entries = {}

bitmaps.each do |cp, bm|
  lines = bm.scan /.{#{ARGV[2].to_i}}/
  this_entry = []
  this_byte = 0
  byte_mask = 1
  for y in 0..(ARGV[4].to_i() - 1) do
    for x in 0..(ARGV[3].to_i() - 1) do
      if lines[y][x] == '1'
        this_byte |= byte_mask
      end
      byte_mask *= 2
      if byte_mask == 256
        byte_mask = 1
        this_entry << this_byte
        this_byte = 0
      end
    end
  end
  if byte_mask != 1
    this_entry << this_byte
  end
  data_area_entries[cp] = this_entry
end

data_area = []

def put_u32(f, n)
  f.putc (n % 256)
  f.putc ((n / 256) % 256)
  f.putc ((n / 65536) % 256)
  f.putc ((n / 16777216) % 256)
end

File.open(ARGV[1], 'wb') do |f|
  f.putc ARGV[3].to_i
  f.putc ARGV[4].to_i
  f.putc ARGV[5].to_i
  f.putc ARGV[6].to_i

  for cp in 0..255 do
    if data_area_entries.key? cp
      put_u32(f, data_area.length)
      for b in data_area_entries[cp] do
        data_area << b
      end
    else
      put_u32(f, 0)
    end
  end

  for b in data_area do
    f.putc b
  end
end